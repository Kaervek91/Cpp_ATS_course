/* 
    Queues : Ex1 : Create a program using Queues which has the next menu
    1. Inserte a character to the stack
    2. Show all elements of the stack
    3. Exit
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

struct Nodo{
    Nodo *next;
    int value;
};

int printQueueNode(Nodo *& pila, short next);
void printQueueComplete(Nodo *&pila);
void addNodeToQueue(Nodo *&front,Nodo *&end, int n);
void delNodeFromQueue(Nodo *&front, Nodo *&end, int &n);
void printQueue(Nodo *&front,Nodo *&end);
void menu(short &val);

int main(){
    short exit = 0;
    short selector = 0;
    int number;
    char printAll = 0;
    Nodo *front = NULL;
    Nodo *end = NULL;

    do{
        exit = 0;
        menu(selector);
        switch(selector){
            case 1:
                cout << "Introduce the number: " ; cin >> number; cout << endl;
                addNodeToQueue(front,end, number);
                break;
            case 2:
                if (end != NULL && front != NULL){
                    cout<< "Deleting first number entered in the queue" << endl;
                    delNodeFromQueue(front,end,front->value);
                    cout << "Deleted" << endl;
                } else {
                    cout << "Nothing to erase" << endl;
                }
                break;
            case 3:
                if (end != NULL && front != NULL){
                    cout << "Printing values" << endl;
                    printQueue(front,end);
                }else{
                    cout << "No values to print at all" << endl;
                }
                break;
            case 4:
                
                if (end != NULL && front != NULL){
                    cout << "Deleting all" << endl;
                    while (end != NULL && front != NULL) {
                        cout << "Deleting " << front->value << endl;
                        delNodeFromQueue(front, end,front->value) ;
                    };
                    
                } else{
                    cout << "Nothing to erase" << endl;
                }
                
                break;                
            case 5:
                cout << "Bye bye my friend!" << endl;
                exit = 1;
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
        
    }while (!exit);
    
    
    return 0;

}


void menu(short &val){
    cout << "Choose one option please:\n" << \
        "\t 1. Insert one character to the stack\n" <<   \
        "\t 2. Delete last character from the stack\n" <<  \
        "\t 3. Print all\n" <<  \
        "\t 4. Delete all\n" <<  \
        "\t 5. Exit\n"  ;
        cin >> val;
}

void addNodeToQueue(Nodo *&front,Nodo *&end, int n){
    Nodo *new_node = new Nodo();
    new_node->value = n;
    new_node->next= NULL;
    if (front == NULL){
        front = new_node;
    } else {
        end->next = new_node;
    }
    end = new_node;
}
void delNodeFromQueue(Nodo *&front, Nodo *&end, int &n){
    n = front->value;
    Nodo *aux= front;
    if (front == end){
        front = NULL; end = NULL;
    } else {
        front = front ->next;
    }
    delete aux;
}


void printQueue(Nodo *&front,Nodo *&end){
    if (front!=NULL && end!= NULL){
        Nodo *aux;
        cout << front->value << " ";
        aux = front->next;
        printQueue(aux,end);
    } else {
        cout << "\n";
    }
    
    
}



