/* 
    Queues : Ex2 : Create a bank program using Queues which stores the values of clients from a bank and then show the clients in correct order.
    1. Inserte a character to the stack
    2. Show all elements of the stack
    3. Exit
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

const string names[6] = {"Manuel","Pedro","Hugo","Lorena","Sabela","Isabel"};
const string surnames[6] = {"Mallo","Orihuela","Méndez","Alonso","Montenegro","Saba"};
const string gender[2] = {"Varón","Mujer"};
const string subjects[5]={"Física","Química","Matemáticas","Lengua","Filosofía"};

struct Account{
    double id;
    float money;
};

struct User{
    string name;
    string surname;
    int age;
    string sex;
    Account account;
    int order; 
};

struct Nodo{
    Nodo *next;
    User user;
};

int printQueueNode(Nodo *& pila, short next);
void printQueueComplete(Nodo *&pila);
void addNodeToQueue(Nodo *&front,Nodo *&end, User n);
void delNodeFromQueue(Nodo *&front, Nodo *&end, User &n);
void printQueue(Nodo *&front,Nodo *&end);
void menu(short &val);
float RandomFloat(float a, float b);
void printUser(User user);
void CreateUser(User &User);

int main(){
    short exit = 0;
    short selector = 0;
    User GeneralUser;
    char printAll = 0;
    Nodo *front = NULL;
    Nodo *end = NULL;

    do{
        exit = 0;
        menu(selector);
        switch(selector){
            case 1:
                cout << "Introduce the User: " ;
                CreateUser(GeneralUser);
                addNodeToQueue(front,end, GeneralUser);
                break;
            case 2:
                if (end != NULL && front != NULL){
                    cout<< "Deleting first number entered in the queue" << endl;
                    delNodeFromQueue(front,end,front->user);
                    cout << "Deleted" << endl;
                } else {
                    cout << "Nothing to erase" << endl;
                }
                break;
            case 3:
                if (end != NULL && front != NULL){
                    cout << "Printing values" << endl;
                    printQueue(front,end);
                    
                }else{
                    cout << "No values to print at all" << endl;
                }
                break;
            case 4:
                
                if (end != NULL && front != NULL){
                    cout << "Deleting all" << endl;
                    while (end != NULL && front != NULL) {
                        cout << "Deleting " << front->user.name << " " << front->user.surname  << endl;
                        delNodeFromQueue(front, end,front->user) ;
                    };
                    
                } else{
                    cout << "Nothing to erase" << endl;
                }
                
                break;                
            case 5:
                cout << "Bye bye my friend!" << endl;
                exit = 1;
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
        
    }while (!exit);
    
    
    return 0;

}


void menu(short &val){
    cout << "Choose one option please:\n" << \
        "\t 1. Insert one character to the stack\n" <<   \
        "\t 2. Delete last character from the stack\n" <<  \
        "\t 3. Print all\n" <<  \
        "\t 4. Delete all\n" <<  \
        "\t 5. Exit\n"  ;
        cin >> val;
}

void addNodeToQueue(Nodo *&front,Nodo *&end, User n){
    Nodo *new_node = new Nodo();
    new_node->user = n;
    new_node->next= NULL;
    if (front == NULL){
        front = new_node;
    } else {
        end->next = new_node;
    }
    end = new_node;
}
void delNodeFromQueue(Nodo *&front, Nodo *&end, User &n){
    n = front->user;
    Nodo *aux= front;
    if (front == end){
        front = NULL; end = NULL;
    } else {
        front = front ->next;
    }
    delete aux;
}


void printQueue(Nodo *&front,Nodo *&end){
    if (front!=NULL && end!= NULL){
        Nodo *aux;
        //cout << front->user. << " ";
        printUser(front->user);
        aux = front->next;
        printQueue(aux,end);
    } else {
        cout << "\n";
    }
    
    
}

void CreateUser(User &User){
    int nameN =  (rand() % 6);
    User.name = names[nameN];
    User.surname = surnames[(rand() % 6)];
    User.sex = (nameN <= 3) ? gender[0] : gender[1];
    User.age = rand() % 80;
    User.account.id = rand() % 9999999999;
    User.account.money = RandomFloat(0,99999999999);
}

float RandomFloat(float a, float b){
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void printUser(User user){
    cout << "Printing user data \n";
    cout << user.name << endl;
    cout << user.surname << endl;
    cout << user.sex << endl;
    cout << user.age << endl;
    cout << user.account.id << endl;
    cout << user.account.money << endl;
}