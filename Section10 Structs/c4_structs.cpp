/* 
    Structs Exercise 4 Make an struct called Athlete with teh next fields:
    Name, country, number of medals, and return the data from the best of the best.

    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??
#include <time.h>

using namespace std;

const string names[6] = {"Manuel","Pedro","Lorena","Hugo","Sabela","Isabel"};
const string surnames[6] = {"Mallo","Orihuela","Méndez","Alonso","Montenegro","Saba"};
const string competitions[6] = {"Juegos Olímpicos","Mundial","Global Energy Race Badalona","VigBai","Camino de Santiago","Rodrigo Marañón"};
const string countries[6] = {"España","Marruecos","Alemania","USA","Francia","Portugal"};

struct Badge {
    string competition;
    time_t dateUTC;
    int position;
};

struct Athlete {
    string name;
    string surname;
    string country;
    int age;
    Badge * collectionBadge;
    int totalBadges;

} BadgePrototype;

struct Competitors{
    Athlete * collectionAthletes;
} Competition, * pComp = &Competition;

int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols);
void FreeMemory(int ** matrix,int Rows, int Cols);
int ** BuildShowMatrix(int ** matrix,int Rows, int Cols);
int ** Traspuesta(int ** matrix,int Rows, int Cols);
void PrintArray(int * array, int * length);
int   AnalyzeArray(char * array, int * length, int * vowelP);
int *  OrderArray(int * array, int * length);
int * FindNumber(int number,int * array, int * length);
void search(int a[],int number,int first, int last);


int main(){
    int numberOfAthletes = 0;
    
    cout << "Introduce el número de atletas a registrar " ; cin >> numberOfAthletes;
    pComp->collectionAthletes = new Athlete [numberOfAthletes];
    /* Autocompletar Atletas */
    for (int i = 0 ; i<numberOfAthletes; i++){
        pComp->collectionAthletes[i].name = names[(rand() % 6)];
        pComp->collectionAthletes[i].surname = surnames[(rand() % 6)];
        pComp->collectionAthletes[i].country = countries[(rand() % 6)];
        pComp->collectionAthletes[i].age = rand() % 50;
        pComp->collectionAthletes[i].totalBadges = rand() % 5 + 1;
        pComp->collectionAthletes[i].collectionBadge = new Badge [pComp->collectionAthletes[i].totalBadges];
        /* Autocompletar medallas*/
        for (int j = 0 ; j < pComp->collectionAthletes[i].totalBadges; j++){
            pComp->collectionAthletes[i].collectionBadge[j].competition = competitions[rand() % 6];
            //time (&(pComp->collectionAthletes[i].collectionBadge[j].dateUTC)) ;
            pComp->collectionAthletes[i].collectionBadge[j].dateUTC = rand() % 1563030000 + 300000;
            pComp->collectionAthletes[i].collectionBadge[j].position = rand() % 3; 
        }
        
    }
    /* PRINT ALL */
    for (int i = 0; i< numberOfAthletes;i++){
        cout << "  ---------------------------- Athlete number " << (i+1) << " ----------------------------" << endl;
        cout << "Name and surname " << pComp->collectionAthletes[i].name << " " << pComp->collectionAthletes[i].surname << endl; 
        cout << "Age " << pComp->collectionAthletes[i].age << endl;
        cout << "Country " << pComp->collectionAthletes[i].country << endl;
        
        for (int j = 0; j < pComp->collectionAthletes[i].totalBadges;j++){
            cout << "  ---------------------------- Badge number " << (j+1) << " ----------------------------" << endl;
            cout << "Competition " << pComp->collectionAthletes[i].collectionBadge[j].competition << endl;
            cout << "Position " << pComp->collectionAthletes[i].collectionBadge[j].position << endl;;
            cout << "Date " << asctime(localtime(&(pComp->collectionAthletes[i].collectionBadge[j].dateUTC))) << endl;
            cout << "  ---------------------------- Badge number " << (j+1) << " ----------------------------" << endl;
        }
        cout << "  ---------------------------- Athlete number end " << (i+1) << " ------------------------" << endl;

    }
    /* Find the best and print out*/
    Athlete * TheBest = NULL;
    for (int i=0; i< numberOfAthletes ;i++){
        if (i == 0){
            TheBest = &pComp->collectionAthletes[0];
        }else {
            if (TheBest->totalBadges <  pComp->collectionAthletes[i].totalBadges){
                TheBest = &pComp->collectionAthletes[i];
            }
        }       
    }
    /* PRINT THE BEST */
    cout << "This Man records are ... " << endl;
    cout << "Name and surname " << TheBest->name << " " << TheBest->surname << endl; 
        cout << "Age " << TheBest->age << endl;
        cout << "Country " << TheBest->country << endl;
        
        for (int j = 0; j < TheBest->totalBadges;j++){
            cout << "  ---------------------------- Badge number " << (j+1) << " ----------------------------" << endl;
            cout << "Competition " << TheBest->collectionBadge[j].competition << endl;
            cout << "Position " << TheBest->collectionBadge[j].position << endl;;
            cout << "Date " << asctime(localtime(&(TheBest->collectionBadge[j].dateUTC))) << endl;
            cout << "  ---------------------------- Badge number " << (j+1) << " ----------------------------" << endl;
        }
    cout <<  "The Best!!" << endl;
    /*CyclistP->collectionStages = new struct Stage [numberOfStages];

     cout << "La mejor etapa de " << CyclistP->name << endl;
        cout << "Stage Number " << CyclistP->bestStage.stageID << endl ;
        cout << "Distance " << CyclistP->bestStage.distanceKM << endl ; 
        cout << "Avg Speed " << CyclistP->bestStage.avgSpeed << endl;
        cout << "Hours " << CyclistP->bestStage.hours << endl;
        cout << "Minutes " << CyclistP->bestStage.minutes << endl;
        cout << "Seconds " << CyclistP->bestStage.seconds << endl;*/
    return 0;

}


int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols){
    int **result = new int * [Rows];
    for (int i=0; i<Rows; i++){
        result[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(result+i)+j) = *(*(matrix + i)+j) + *(*(matrix2 + i)+j);
        }
    }

    return result;
}

int ** BuildShowMatrix(int ** matrix,int Rows, int Cols){
    matrix = new int * [Rows];
    for (int i=0; i<Rows; i++){
        matrix[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(matrix + i)+j) = rand() % 10;
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            cout << matrix[i][j] << " " ;
        }    
        cout << endl;
        
    }
    return matrix;
}

int ** Traspuesta(int **matrix,int Rows, int Cols){
    int **traspuesta = new int * [Rows];
    
    for (int i=0; i<Rows; i++){
        traspuesta[i] = new int [Cols]; 
    }
    cout << "Pay9o9o" << endl;
    for (int i=0 ;i < Rows; i++){
        for (int j=0;j<Cols;j++){
            
            *(*(traspuesta+j)+i) = *(*(matrix + i)+j);
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            //cout << matrix[i][j] << " " ;
            cout << traspuesta[i][j] << " ";
        }   
        cout << endl;
    }
    return traspuesta;
}

void FreeMemory(int ** matrix,int Rows, int Cols){
    
    for (int i=0; i<Rows; i++){
        delete[] *(matrix + i);
        //cout << i<< endl;
    }
    delete[] matrix;
}

int  AnalyzeArray(char * array, int * length, int * vowelP){
    int * result = new int;

    for (int i = 0 ; i < *length; i++) {
        //cout << *(array+i) << endl;
        switch (*(array + i)){
            case 'a':
                (*result)++;
                (*vowelP)++;
                break;
            case 'e':
                (*result)++;
                (*(vowelP+1))++;
                break;
            case 'i':
                (*result)++;
                (*(vowelP+2))++;
                break;
            case 'o':
                (*result)++;
                (*(vowelP+3))++;
                break;
            case 'u':
                (*result)++;
                (*(vowelP+ 4))++;
                break;
            default:
                break;
        }
        
    }
    cout << result << " " << *result << endl;
    
    return *result;
}


int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}


void intercambio(int &a, int &b){
    int aux;
    aux = a;
    a = b;
    b = aux;
}

void search(int a[],int number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}
void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

