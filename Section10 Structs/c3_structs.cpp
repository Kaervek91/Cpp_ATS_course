/* 
    Ejercicio Structs , hacer una arreglo de estructuras los datos de N workers de la empresa e imprima los datos del worker 
    en la cual se tendran los siguientes campos: Nombre, edad, salario. pedir datos al usuario para 3 alumnos
    y comprobar cuál de los 3 tiene el menor y mayor salario y posteriormente imprmir los datos del usuario
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <vector>
#include <tuple>


using namespace std;

struct worker {
    string name;
    int age;
    int salary;
};

tuple <worker *,worker *> checkMaxMinSalary(worker *bestPaidP, worker *worstPaidP, vector<worker> &vect);
void printPointer(worker &vect);
void printVector(vector<worker> const &vect);
void print(float number[], int n);
void search(float a[],float number,int first, int last);
void quickShort(float a[],int first, int last);
void binarySearch(float *array,float number,int inf,int sup);


int main(){

    int n;
    vector<worker> empresa;
    cout << "Insert number of workers to save" << " "; cin >> n;
    for (int i = 0 ; i < n; i++ ){
        worker aux;
        cout << "Complete the next formulary for worker " << i << endl;
        fflush(stdin);
        cout << "Insert worker name "; cin >> aux.name; cout << endl;
        fflush(stdin);
        cout << "Insert worker age "; cin >> aux.age; cout << endl;
        fflush(stdin);
        cout << "Insert worker salary "; cin >> aux.salary; cout << endl;
        empresa.push_back(aux);
    };
    worker *bestPaid = &empresa[0];
    worker *worstPaid = &empresa[0];
    //cout << &empresa << endl;;
    tie(bestPaid,worstPaid) = checkMaxMinSalary(bestPaid,worstPaid,empresa);
    cout << "Best Worker Salary" << endl;
    printPointer(*bestPaid);
    cout << "Worst Worker Salary" << endl;
    printPointer(*worstPaid);
    return 0;

}

void printPointer(worker &vect){
    cout << vect.name << endl;
    cout << vect.age << endl;
    cout << vect.salary << endl;
}

void printVector(vector<worker> const &vect){
    
    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i).name << endl;
        cout << vect.at(i).age << endl;
        cout << vect.at(i).salary << endl;
    }
}
// Pending to solve pointer actions
tuple <worker *,worker *> checkMaxMinSalary( worker *bestPaid, worker *worstPaid, vector<worker> &vect){
    
    for (int i=0; i < vect.size(); i++){
        if (bestPaid->salary < vect.at(i).salary) {
            bestPaid = &vect.at(i);
        }
        if (worstPaid->salary > vect.at(i).salary) {
            worstPaid = &vect.at(i);
        }
        
    }
    //cout << "Best Paid Salary " << bestPaid->salary << endl;
    //cout <<"Worst Paid Salary" <<  worstPaid->salary << endl;
    return make_tuple(bestPaid,worstPaid);
}
void print (float number[], int n){   
    int i;
    for (i=0;i<=n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void search(float a[],float number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}

void quickShort(float a[],int first, int last){
    int middle,i,j;
    float pivot;
    middle = (first + last )/2;
    pivot = a[middle];
    i = first;
    j = last;

    do{
        while(a[i]< pivot) i++;
        while(a[j]> pivot) j--;

        if (i<=j){
            intercambio(a[i], a[j]);
            i++;
            j--;
        }
    }while(i<=j);
    
    if (first < j){
        quickShort(a,first,j);
    }

    if (i < last){
        quickShort(a,i,last);
    }

}

void binarySearch(float *array,float number,int inf,int sup){
    int mitad,found = 0;
    
    while (inf <= sup){
        mitad = (inf+sup)/2;
        if (array[mitad] == number){
            found = 1;
            inf = mitad+1;
            break;
        }
        if (array[mitad] > number){
            sup = mitad;
            mitad = (inf+sup)/2;
        }
        if (array[mitad] < number){
            inf = mitad;
            mitad = (inf+sup)/2;
        }
    }
    if (found) {
        cout << "Number Found in position " << mitad << " with value " << number << endl;
        
    }
}