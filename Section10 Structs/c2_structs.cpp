/* 
    Ejercicio Structs , hacer una estructura llamada alumno en la cual se tendran los siguientes campos: Nombre, edad, promedio. pedir datos al usuario para 3 alumnos
    y comprobar cuál de los 3 tiene el mejor promedio y posteriormente imprmir los datos del usuario
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <vector>

using namespace std;

struct alumno {
    string name;
    int age;
    int avg;
};

vector<alumno> checkAvg(vector<alumno> const &vect);
void printVector(vector<alumno> const &vect);
void print(float number[], int n);
void search(float a[],float number,int first, int last);
void quickShort(float a[],int first, int last);
void binarySearch(float *array,float number,int inf,int sup);


int main(){

    int n;
    vector<alumno> clase;
    vector <alumno> bestStudent;
    cout << "Insert number of students to save" << " "; cin >> n;
    for (int i = 0 ; i < n; i++ ){
        alumno aux;
        cout << "Complete the next formulary for student " << i << endl;
        fflush(stdin);
        cout << "Insert student name "; cin >> aux.name; cout << endl;
        fflush(stdin);
        cout << "Insert student age "; cin >> aux.age; cout << endl;
        fflush(stdin);
        cout << "Insert student average "; cin >> aux.avg; cout << endl;
        clase.push_back(aux);
    };
    bestStudent = checkAvg(clase);
    cout << "Best Student Data" << endl;
    printVector(bestStudent);
    
    return 0;

}

void printVector(vector<alumno> const &vect){
    
    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i).name << endl;
        cout << vect.at(i).age << endl;
        cout << vect.at(i).avg << endl;
    }
}

vector<alumno> checkAvg(vector<alumno> const &vect){
    vector<alumno> bestStudent;
    int avg = 0;
    for (int i=0; i < vect.size(); i++){
        if (avg < vect.at(i).avg) {
            bestStudent.clear();
            bestStudent.push_back(vect.at(i));
        }
    }
    return bestStudent;
}
void print (float number[], int n){   
    int i;
    for (i=0;i<=n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void search(float a[],float number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}

void quickShort(float a[],int first, int last){
    int middle,i,j;
    float pivot;
    middle = (first + last )/2;
    pivot = a[middle];
    i = first;
    j = last;

    do{
        while(a[i]< pivot) i++;
        while(a[j]> pivot) j--;

        if (i<=j){
            intercambio(a[i], a[j]);
            i++;
            j--;
        }
    }while(i<=j);
    
    if (first < j){
        quickShort(a,first,j);
    }

    if (i < last){
        quickShort(a,i,last);
    }

}

void binarySearch(float *array,float number,int inf,int sup){
    int mitad,found = 0;
    
    while (inf <= sup){
        mitad = (inf+sup)/2;
        if (array[mitad] == number){
            found = 1;
            inf = mitad+1;
            break;
        }
        if (array[mitad] > number){
            sup = mitad;
            mitad = (inf+sup)/2;
        }
        if (array[mitad] < number){
            inf = mitad;
            mitad = (inf+sup)/2;
        }
    }
    if (found) {
        cout << "Number Found in position " << mitad << " with value " << number << endl;
        
    }
}