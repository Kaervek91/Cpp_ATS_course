//Tipos de datos básicos de C++

#include <iostream>

using namespace std;

int main () {

    int entero = 5;
    float flotante = 10.45;
    double mayor = 16.3456;
    char caracter = 'a';

    cout << "Número entero : " << entero <<endl;
    cout << "Número flotante : " << flotante <<endl;
    cout << "Número de mayor longitud : " << mayor <<endl;
    cout << "Expresión de caracter : " << caracter <<endl;

    return 0;
}
