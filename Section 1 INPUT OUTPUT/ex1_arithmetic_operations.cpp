/*
    Exercise 1: Write a program that reads the standard input of two numbers and shows an output from its sum, substract, multiplication and divide both
*/

#include <iostream>

using namespace std;

int main () {

    float number1;
    float number2;
    cout<<"Enter number 1: ";
    cin >> number1;
    cout<<"\nEnter number 2: ";
    cin >> number2;
    float sum = number1 + number2;
    float rest = number1 - number2;
    float multiplication = number1 * number2;
    float division = number1/number2;
    cout<< "\nSeveral arithmetic operation:\n";
    cout << "Sum : " << sum <<endl;
    cout << "Rest : " << rest <<endl;
    cout << "Multiplication : " << multiplication <<endl;
    cout << "Division : " << division <<endl;

    return 0;
}