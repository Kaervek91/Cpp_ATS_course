/*
    Exercise 2 : Write a program where reads the price input of a product and applies a 21% of taxes to the final price of the product
*/

#include <iostream>

using namespace std;

int main() {
    float priceB4Taxes;
    float finalPrice;

    cout << "Introduce the value of the product: ";
    cin >> priceB4Taxes;
    finalPrice = priceB4Taxes*1.21;
    cout << "\nFinal price : " << finalPrice << " €\n";



    return 0;
}