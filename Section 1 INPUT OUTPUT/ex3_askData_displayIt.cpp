/*
    Ask the following data:
        AGE
        SEX
        HEIGHT
    Display it properly
*/

#include <iostream>

using namespace std;


int main () {

    int age;
    string sex;
    float height;

    cout<< "Enter your age: ";
    cin >> age;
    cout<< "\nEnter your sex (male/female) : ";
    cin >> sex;
    cout<< "\nEnter your height (meters): ";
    cin >> height;

    cout<< "Your age is " << age << " your sex is " << sex << " and your height is " << height << " meters" << endl;


    return 0;


}
