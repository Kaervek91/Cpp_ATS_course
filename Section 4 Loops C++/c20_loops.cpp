/* 
    Factorize a number in prime factors
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <math.h>

using namespace std;

const int lowerLimit = 0;
const int upperLimit = 100;
int RandomInt();
bool isPrime(int number);
void printValues(vector<int> array, int number);


int main(){
    int number;
    cout << "Give me a number to factorize: " << endl; cin >> number;
    int aux = number;
	vector<int> factorsContainer;
	for (int i = 2; i <= number;i++){
		if (isPrime(i)){
			while(aux%i == 0){
				factorsContainer.push_back(i);
				aux = aux/i;
			}
		}
	}
    cout << "Size of Vector: " << factorsContainer.size() << endl;
    printValues(factorsContainer,number);
    system("pause");
    return 0;
}

void printValues(vector<int> array, int number){
	cout << "Print Values contained: ";
	cout << " " << number << " = ";
	    for (auto z = array.begin(); z != array.end(); ++z){
	    	if (z != array.end()-1){
	    		cout << *z << '*' ;
	    	}else {
	    		cout << *z << endl ;
	    	}

	    }
}

bool isPrime(int number){
	int i;
	bool isPrime = true;
	cout << " IsPrime function with iteration number " << number;
	for(i = 2; i <=number; i++){
	      if(number%i == 0 && (number != i)){
	          isPrime = false;
	          cout << ", but it is not prime" << endl;
	          break;
	      }
	  }
	if (isPrime) cout << ", it is prime" << endl;
	return isPrime;
}

int RandomInt(){

    int diff  = upperLimit - lowerLimit;
    int randomNumber = lowerLimit + rand() % (diff + 1);
    cout << "Random Number " << randomNumber;
    return randomNumber;
}
