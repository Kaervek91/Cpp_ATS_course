/* 
    Class of 150 students which have done 3 examens and want to display the number of:
    * Students that passed all
    * Students that pass at least one 
    * Students that  pass only the last exam
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

float RandomFloat();
int** marksGenerator(int students, int exams);
void filterDisplayer(int** array,int students,int exams);


int main(){
    int students = 0, exams = 0;
    cout << """Class of X students which have done Y examens and want to display the number of:"""
            """* Students that passed all"""
            """* Students that pass at least one """
            """* Students that  pass only the last exam""" << endl;
    cout << "Number of students? " ; cin >> students;
    cout << "Number of exams? " ; cin >> exams;
    int** array = marksGenerator(students,exams);
    filterDisplayer(array, students, exams);
    cout << "THE END" << endl;
    students = getchar(); 
    return 0;
}

void filterDisplayer(int** array,int students,int exams){
    int i, z, passAll = 0, passOne = 0, passLast = 0;
    int countPass = 0;
    int countPassOne = 0;
    for (i=0;i<students; i++){
        cout << "\nStudent "<< i << endl;
        countPass = 0;
        for (z=0;z< exams; z++){
            cout << "Exam " << z << "\t" << array[i][z] << endl;
            if (array[i][z] >= 5) {
                countPass++;
                if(z == (exams-1) && countPass == 1){
                    passLast++;
                }
            }
        }
        if (countPass == exams) passAll++;
        if (countPass == 1) passOne++;
    }
    cout << "Students that passed all: "<< passAll << endl;
    cout << "Students that passed at least one: "<< passOne << endl;
    cout << "Students that passed only the last exam: "<< passLast << endl;
}

int** marksGenerator(int students, int exams){
    int i,z;
    int** result = 0;
    result = new int*[students];
    for (i=0;i<students;i++){
        result[i] = new int[exams];
        for(z=0;z<exams;z++){
            result[i][z] = RandomFloat();
        }
    }
    return result;
}

float RandomFloat(){
    float a = 0;
    float b = 10;
    float random = ((float)rand())/(float) RAND_MAX;
    float diff  = b-a;
    float r = random * diff;
    return a+r;
}
