/* 
    Calculate the sequence 1-2+3-4+5-6...n
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

int calculate (int value);

int main(){
    int number = 0;
    int i;
    double total=0;
    cout << "Calculate the sequence 1-2+3-4+5-6...n: "; cin >> number;
    for (i=1; i< number+1; i++){
        total+=calculate(i);      
    }
    cout << "The number entered is: " << number << endl;
    cout << "The result is " << total << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

int calculate(int value){
    int result;
    if (value%2 == 0){
        value = -value;
    } 
    return value;
}

