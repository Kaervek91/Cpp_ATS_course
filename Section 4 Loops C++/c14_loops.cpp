/* 
    Calculate the sequence 1!+2!+3!+...+n!
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

int factorize (int value);

int main(){
    int number = 0;
    int i;
    double total=0;
    cout << "Calculate the sequence 1!+2!+3!+...+n!: "; cin >> number;
    for (i=1; i< number+1; i++){
        total+=factorize(i);      
    }
    cout << "The number entered is: " << number << endl;
    cout << "The result is " << total << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

int factorize (int value){
    int i,result;
    result = value;
    for (i=0;i<value+1;i++){
        value--;
        if (value >0) {
            result*=value;
        }
    }
    return result;
}

