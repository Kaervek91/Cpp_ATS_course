/* 
   Ask the user for an integer number between 1-100.The program should generate a random number between the range
            and told him if it is greater or lower and give him another try
            until it reach the solution
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

using namespace std;

const int lowerLimit = 0;
const int upperLimit = 100;
int RandomInt();


int main(){
    int number, randomNumber , contador = 0;
    bool notGuess = true;
    cout << """Ask the user for an integer number between 1-100."""
            """The program should generate a random number between the range"""
            """and told him if it is greater or lower and give him another try"""
            """until it reach the solution""" << endl;
    randomNumber = RandomInt();
    do{
    	cout << "Try to guess the number? " ; cin >> number;
    	//if (!isnan(number)){
    		if (number>= lowerLimit && number <= upperLimit) {
    			if(number > randomNumber){
    				cout << "Your number is higher" << endl;
    			} else if ( number < randomNumber){
    				cout << "Your number is lower" << endl;
    			}
    		} else{
    			cout << "The value entered is not between the range" << endl;
    		}
    	//} else{
    		//cout << "The value entered is not a number" << endl;
    	//}
    }while(number != randomNumber);

    cout << "CHEERS!!!" << endl;
    cout << "You guessed it!!!" << endl;
    system("pause");
    return 0;
}



int RandomInt(){

    int diff  = upperLimit - lowerLimit;
    int randomNumber = lowerLimit + rand() % (diff + 1);
    cout << "Random Number " << randomNumber;
    return randomNumber;
}
