/* 
    Calculate FIBONACCI SEQUENCE 1,1,2,3,5,8,13,...n
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

double fibonacci (double value);
//string fibonacci (int value);

int main(){
    double number = 0;
    cout << "Calculate FIBONACCI SEQUENCE 1,1,2,3,5,8,13,...n: "; cin >> number;
    cout << "The number entered is: " << number << endl;
    fibonacci(number);
    //cout << "The result is " << fibonacci(number) << endl;
    cout << "THE END" << endl;
    number = getchar(); 
    return 0;
}

double fibonacci(double value){
    double result = 1;
    cout << result << ", ";
        
    double i, x = 0, y = 1, z=1;
    for (i=1; i< value+1; i++){
        z = x+y;
        //result.append(to_string(z)+ ", ");    
        cout << z << ", ";
        x = y;
        y = z;
    }
    return result;
}

