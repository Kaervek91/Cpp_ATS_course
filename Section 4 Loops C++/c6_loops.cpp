/* 
    Ask for integer numbers and show in the output the sume of the square of the first 10 integers bigger than 0

    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String

*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

int main(){
    int i;
    int number;
    int squareSum = 0;
    int counterPositive = 0;
    int recorder[10];
    do{
        cout << "Enter an integer number "; cin >> number; cout << endl;
        if (number > 0) {
            recorder[counterPositive] = number;
            counterPositive++;
        }
    } while (counterPositive < 10);
    cout << "The positive first 10 numbers entered are: " << endl;
    for(i=0;i<10;i++){
        cout << recorder[i] << " and its square " << pow(recorder[i],2) << endl;
        squareSum += pow(recorder[i],2);
        if (i<9){
            cout << "\t\t +\n";
        }
    }
    cout << "RESULT \t\t = " << squareSum << endl;
    cout << "THE END" << endl;
    i = getchar(); //Proviene del stdio.h y obtengo un resultado casi igual al getch de conio.h
    //system("pause"); Proviene del stdlib.h y no funciona en linux pero sí en Windows
    //cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}