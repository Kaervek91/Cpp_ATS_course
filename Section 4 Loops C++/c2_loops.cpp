/* DO + While Sentence
    Print values from 1 to 10
*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler
#include <stdlib.h>
#include <stdio.h>
using namespace std;

int main(){

    int x = 1;
    do {
        cout << "Valor de X:" << x << endl;
        x++;
    } while(x <=10);
    x = getchar(); //Provioene del stdio.h y obtengo un resultado casi igual al getch de conio.h
    //system("pause"); Proviene del stdlib.h y no funciona en linux pero sí en Windows
    //cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}