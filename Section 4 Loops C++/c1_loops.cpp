/* While Sentence
    Print values from 1 to 10
*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler

using namespace std;

int main(){

    int x = 1;
    while(x <=10){
        cout << "Valor de X:" << x << endl;
        x++;

    }
    cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}