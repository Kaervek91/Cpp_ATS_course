/* 
    Ask for an integer numbers until you receive a 0 then exits the program and show the numbers remembered and count the numbers greater than 0

    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String

*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(){
    int i;
    int number;
    int counterPositive = 0;
    string recorder = "";
    do{
        cout << "Enter an integer number "; cin >> number; cout << endl;
        if (number > 0) counterPositive++;
        if (number != 0){
            std::string aux = " ";
            aux +=to_string(number);
            recorder.append(aux);
            aux = " ";
        }
    } while (number != 0);
    cout << "The numbers entered are in the following order: " << recorder << endl;
    cout << "Number of positive numbers counted " << counterPositive << endl;
    cout << "THE END" << endl;
    i = getchar(); //Proviene del stdio.h y obtengo un resultado casi igual al getch de conio.h
    //system("pause"); Proviene del stdlib.h y no funciona en linux pero sí en Windows
    //cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}