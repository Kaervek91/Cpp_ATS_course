/* 
    Ask for an integer number from 1 to 10 and show standard ouptut its multiplication table
*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler
#include <stdlib.h>
#include <stdio.h>
using namespace std;

int main(){
    int i;
    int number;
    do{
        cout << "Enter a number between 1 - 10 to show its multiplication table ";cin >> number; cout << endl;
    } while (number >=10 && number <=1);
    cout << "MULTIPLICATION TABLE OF " << number << endl;
    for (i=1;i<=10;i++){
        int result = number * i;
        cout << i << " * " << number << " = " << result << endl;  
    }
    cout << "THE END" << endl;
    i = getchar(); //Provioene del stdio.h y obtengo un resultado casi igual al getch de conio.h
    //system("pause"); Proviene del stdlib.h y no funciona en linux pero sí en Windows
    //cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}