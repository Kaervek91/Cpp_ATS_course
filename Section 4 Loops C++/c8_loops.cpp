/* 
    Read ingegers values entered until it receives a value between [20-30] or a 0.
    Then sums all the numbers greater than 0.
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

float RandomFloat();

int main(){
    int i;
    int number;
    int totalPositive = 0;
    string recorder = "";
    do{
        number = RandomFloat();
        if (number > 0) totalPositive +=number;
        if (number != 0){
            std::string aux = " ";
            aux +=to_string(number);
            recorder.append(aux);
            aux = " ";
        }
    } while (number != 0 || (number < 30 && number >20));
    cout << "The numbers entered are in the following order: " << recorder << endl;
    cout << "Total Sum of Positive Numbers counted " << totalPositive << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

float RandomFloat(){
    float a = -50;
    float b = 50;
    float random = ((float)rand())/(float) RAND_MAX;
    float diff  = b-a;
    float r = random * diff;
    return a+r;
}
