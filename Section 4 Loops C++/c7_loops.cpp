/* 
    Check every 4 hours the temperature reading in 24h. It must read 6 temperatures
    Calculate average temperature of the day, the higher and lower temperature
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
//#include <conio.h> //Console Input Output library // But gnu compalirer hasn't this library so you can't use it unless you have a Borland Compiler
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <chrono>
#include <thread>

using namespace std;

float RandomFloat();
float AverageTemp(float *temperature, int arraylength );
float maxTemp(float* temperature, int arraylength);
float minTemp(float* temperature, int arraylength);

int main(){
    int i;
    int counter = 0;
    float temperature[6];
    float averageTemperature = 0;
    float highTemperature = 0;
    float lowTemperature = 0;
    int arrayLength = sizeof(temperature)/sizeof(temperature[0]);
    do{
        this_thread::sleep_for(chrono::seconds(1));
        temperature[counter] = RandomFloat();
        cout << "The actual temperature is " << temperature[counter] << endl;
        counter++;
    } while (counter < 6);
    for (i=0;i<6;i++){
        cout << "The temperature in time " << i << " is " << temperature[i] << endl;
    }
    cout << "The average temperature " << AverageTemp(temperature,arrayLength)<< endl;
    highTemperature = maxTemp(temperature,arrayLength);
    lowTemperature = minTemp(temperature, arrayLength);
    cout << "The higher temperature " << highTemperature  << endl; 
    cout << "The lower temperature " << lowTemperature  << endl; 
    cout << "THE END" << endl;
    i = getchar(); //Proviene del stdio.h y obtengo un resultado casi igual al getch de conio.h
    //system("pause"); Proviene del stdlib.h y no funciona en linux pero sí en Windows
    //cout << "Press any button to Exit "<< endl; cin >> x; cout << endl;
    //getch(); // Waits until the user press a command to sstep out Function from conio.h>
    return 0;
}

float RandomFloat(){
    float a = 30;
    float b = 36;
    float random = ((float)rand())/(float) RAND_MAX;
    float diff  = b-a;
    float r = random * diff;
    return a+r;
}

float AverageTemp(float* temperature,int arraylength){
    int i;
    float result = 0;
    for (i=0;i<arraylength;i++){
        result += *temperature;
    }
    result = result /arraylength;
    return result;
}

float maxTemp(float* temperature, int arraylength){
    int i;
    float maxAux = *temperature;
    for (i = 1; i<arraylength; i++){
        if (maxAux < temperature[i]){
            maxAux = temperature[i];
        }
    }
    return maxAux;
}

float minTemp(float* temperature, int arraylength){
    int i;
    float minAux= *temperature;
    for (i = 0; i<arraylength; i++){
        if (minAux > temperature[i]){
            minAux = temperature[i];
        }
    }
    return minAux;
}
