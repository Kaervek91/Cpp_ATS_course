/* 
    Calculate the value of 1+2+3+4+5+...+n
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;


int main(){
    int number = 0;
    int i;
    double result;
    cout << "Program to calculate the sum of the sequence of numbers from 1 to n, both included: "; cin >> number;
    for (i=1; i<= number; i++){
        result+=i;
    }
    cout << "The number entered is: " << number << endl;
    cout << "The result is " << result << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

