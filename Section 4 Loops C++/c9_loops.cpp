/* 
    Read 2 ingegers x and y both positives and without pow calculate x^y
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

float RandomFloat();

int main(){
    int countPositive = 0;
    int numbers[2] = {0,0};
    int i;
    double result;
    do{
        int aux = RandomFloat();
        if (aux > 0) {
            numbers[countPositive] = aux;
            countPositive++;  
        }
    } while (countPositive < 2);
    result = numbers[0];
    for (i=0; i< numbers[1]; i++){
        result*=numbers[0];
    }
    cout << "The numbers entered are: " << numbers[0] << " and " << numbers[1] << endl;
    cout << "The result is " << result << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

float RandomFloat(){
    float a = -50;
    float b = 50;
    float random = ((float)rand())/(float) RAND_MAX;
    float diff  = b-a;
    float r = random * diff;
    return a+r;
}
