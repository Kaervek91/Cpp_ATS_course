/* 
    Calculate the value of 1+3+5+...+2n-1
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;


int main(){
    int number = 0;
    int i;
    double result=0;
    cout << "Calculate the following sequence of 1+3+5+...+2n-1: "; cin >> number;
    for (i=1; i<= number; i++){
        result+=2*i-1;
    }
    cout << "The number entered is: " << number << endl;
    cout << "The result is " << result << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

