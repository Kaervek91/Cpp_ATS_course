/* 
    Calculate the mutlipication of the sequece 1*2*3*...*n
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;


int main(){
    int number = 0;
    int i;
    double result=1;
    cout << "Calculate the mutlipication of the sequece 1*2*3*...*n: "; cin >> number;
    for (i=1; i<= number; i++){
        result*=i;      
    }
    cout << "The number entered is: " << number << endl;
    cout << "The result is " << result << endl;
    cout << "THE END" << endl;
    i = getchar(); 
    return 0;
}

