/* 
    Files Store sentences in file
    
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>

using namespace std;

void write();

int main(){

    write();
    return 0;

}

void write(){
    ofstream file;
    string still = "y";
    string file_name = "";
    string sentence = "";
    cout << "Digite el nombre del archivo" << endl;
    getline(cin,file_name);
    file.open(file_name.c_str(),ios::out); //OPEN FILE IF EXISTS, IF NOT CREATE 
    if (file.fail()){
        cout << "No se pudo abrir el archivo" << endl;
        exit(1);
    }
    while (still == "y"){
        cout << "Introduzca su frase ";
        getline(cin,sentence);
        file << sentence;
        cout << "Quiere escribir otra frase? (y/n) ";
        getline(cin,still);
        if (still == "y"){
            file << endl;
        }
     
    }
    

    file.close();
}
