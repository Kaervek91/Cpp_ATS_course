/* 
    Files Create a file and read it later
    
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>

using namespace std;

void write();
void read();
string file_name = "";
string sentence = "";

int main(){
    
    write();
    read();
    return 0;

}

void write(){
    ofstream file; // WRITING MODE
   
    string still = "y";
    
    cout << "Digite el nombre del archivo" << endl;
    getline(cin,file_name);
    file.open(file_name.c_str(),ios::out); //OPEN FILE IF EXISTS it replaces, IF NOT CREATE 
    if (file.fail()){
        cout << "No se pudo abrir el archivo" << endl;
        exit(1);
    }
    while (still == "y"){
        cout << "Introduzca su frase ";
        getline(cin,sentence);
        file << sentence;
        cout << "Quiere escribir otra frase? (y/n) ";
        getline(cin,still);
        if (still == "y"){
            file << endl;
        }
     
    }
    file.close();
    

    
}

void read(){
    ifstream fileR;// Reading mode
    string texto;

    fileR.open(file_name.c_str(),ios::in);
    while(!fileR.eof()){
        getline(fileR,texto);
        cout << texto << endl;
    }
    fileR.close();
}

void add(){

}