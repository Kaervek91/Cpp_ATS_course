/* 
    Files Create a Phone List. To Store phone numbers with the following menu
    1. Create different (name,surname,phonenumber)
    2. Add contacts 
    3. Visualize all
    
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <math.h>

using namespace std;

const string names[6] = {"Manuel","Pedro","Hugo","Lorena","Sabela","Isabel"};
const string surnames[6] = {"Mallo","Orihuela","Méndez","Alonso","Montenegro","Saba"};

struct User{
    string name;
    string surname;
    string phone;
};

string writeUser(User GeneralUser);
void CreateUser(User &User);
void CreatePhoneList(string phoneList);
inline bool existsFile (const string &name);
void readFile(string phoneList);
void addUser(string phoneList,User GeneralUser);
void menu(short &val);

User GeneralUser;
string phoneList;

int main(){
    short exit = 0;
    short selector = 0;
    cout << existsFile("c4_files.txt") << endl;
    cout << existsFile("c4_f2iles.txt") << endl;
    do{
        exit = 0;
        menu(selector);
        switch(selector){
            case 1:
                do {
                    cout << "Introduce name of phone list greater than 4 letters:" ;
                    cin >> phoneList;
                } while(phoneList.length() < 4);
                phoneList+=".txt";
                CreatePhoneList(phoneList);
                break;
            case 2:
                cout << "Introduce User: " ;
                CreateUser(GeneralUser);
                do{
                    cout << "Introduce name of phone list withot extension .txt: ";
                    cin >> phoneList; 
                } while(phoneList.length() < 4);
                phoneList+=".txt";
                addUser(phoneList, GeneralUser);
                break;
            case 3:
                cout << "Introduce name of phone list withot extension .txt: ";
                cin >> phoneList; 
                if (existsFile(phoneList) ==1){
                    readFile(phoneList);
                } else{
                    cout << "File not found!" << endl;
                }

                break;
            case 4:
                cout << "Bye bye my friend!" << endl;
                exit = 1;
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
        
    }while (!exit);

    return 0;

}

void menu(short &val){
    cout << "Choose one option please:\n" << \
        "\t 1. Create a phone list\n" <<   \
        "\t 2. Add a user into a phonelist\n" <<  \
        "\t 3. Print phone list\n" <<  \
        /*"\t 4. Delete all\n" <<  \
        "\t 5. Richest Client\n" <<  \
        "\t 6. Total Money\n" <<  \
        "\t 7. Number of Clients\n" <<  \*/
        "\t 4. Exit\n"  ;
        cin >> val;
}

void CreateUser(User &User){
    int nameN =  (rand() % 6);
    User.name = names[nameN];
    User.surname = surnames[(rand() % 6)];
     
    User.phone = "66666666";
}

void CreatePhoneList(string phoneList){
    ofstream file;
    if (existsFile(phoneList) == 1){
        cout << "El nombre del archivo " << phoneList << " ya existe" << endl;
    } else {
        file.open(phoneList);
        if (file.fail()){
            cout << "No se pudo abrir el archivo" << endl;
        } else {
            cout << "Archivo creado correctamente" << endl;
            file.close();
        }
    }
}

inline bool existsFile (const string &name) {
    ifstream f(name.c_str());
    return f.good();
}

void readFile(string phoneList){
    ifstream fileR;// Reading mode
    phoneList+=".txt";
    string texto;
    if (fileR.fail()){
        cout << "No se pudo abrir el archivo" << endl;
        exit(1);
    } else {
        fileR.open(phoneList.c_str(),ios::out);
        while(!fileR.eof()){
            getline(fileR,texto);
            cout << texto << endl;
        }
        fileR.close();
    }
}

void addUser(string phoneList,User GeneralUser){
    ofstream fileAdd;
    string still = "y";
    fileAdd.open(phoneList.c_str(),ios::app); // Abrimos el archivo en modo añadir
    if (fileAdd.fail()){
        cout << "No se pudo abrir el archivo" << endl;
        exit(1);
    }
    fileAdd << endl;
    fileAdd << writeUser(GeneralUser);
    fileAdd.close();
}

string writeUser(User GeneralUser){
    string text = " User: " + GeneralUser.name + "\n";
    return text;
}