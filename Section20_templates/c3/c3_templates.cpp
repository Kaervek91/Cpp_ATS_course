// Templates C3 programa en C++ Define a class called Subject with data T.

#include <iostream>
#include "subject.h"


using namespace std;

int main(){

    Subject <string,float> subject1("Matemáticas", 5.5);
    Subject <string,int> subject2("Lengua", 6);
    Subject <string,char> subject3("Gimnasia", 'A');
    subject1.getMark();
    subject1.ModMark(3.5);
    subject1.getMark();

    subject2.getMark();
    subject2.ModMark(3);
    subject2.getMark();

    subject3.getMark();
    subject3.ModMark('B');
    subject3.getMark();

    return 0;
}