#include <iostream>


using namespace std;

template <class T1, class T2>
class Subject {
    private:
        T1 IDsubject;
        T2 Mark;
    public:
        Subject(T1 IDsubject, T2 Mark) {
            this->IDsubject = IDsubject;
            this->Mark = Mark;
        }

        void ModMark (T2 NewMark) {
            this->Mark = NewMark;
        }
        void getMark() {
            cout << "In the subject " << this->IDsubject << " you obtained " << this->Mark << endl;
        }
};