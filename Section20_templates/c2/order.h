#include <iostream>
#include "interchange.h"

using namespace std;

template <typename T>
void orderHigh(T *array,int n){
    for (int i=0; i<n; i++) {
        for (int j =0; j< (n-1); j++) {
            if (array[j]>array[i]){
                interchange(array[j],array[i]);
            }
        }
    }
}

template <typename T>
void orderLow(T *array,int n){
    for (int i=0; i<n; i++) {
        for (int j =0; j< (n-1); j++) {
            if (array[j]<array[i]){
                interchange(array[j],array[i]);
            }
        }
    }
}

template <typename T>
void printArray(T *array, int n) {
    for (int i=0; i<n; i++) {
        cout << array[i] << " < " ;
    }
    cout << endl;
}