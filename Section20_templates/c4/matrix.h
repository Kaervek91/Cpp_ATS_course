#include <iostream>


using namespace std;

template <class T>
class Matrix {
    private:
        T * matrix;
        int index;
        int nElem;
    public:
        // Constructor 
        Matrix(int n) {
            this->nElem = n;
            this->matrix = new T[this->nElem];
            this->index = 0;
        }

        void addElem (T element) {
            if (index < nElem) {
                this->matrix[index] = element;
                index++;
            } else {
                cout << "Matrix is out of range it seems full" << endl;
            }
        }

        bool matrixFull() {
            if (index >= nElem) {
                return true;
            } else {
                return false;
            }   
        }

        void showMatrix(){
            for (int i = 0; i< index; i++) {
                cout << "Elemento [" << i << "]: " << this->matrix[i] << endl;
            }
        }

        void emptyMatrix(){
            for (int i = 0; i< index; i++) {
                this->matrix[i] = NULL;
            }
            this->index = 0;
        }

        void getFreeSpace (){
            if (this->nElem> (this->index +1)) {
                cout << "Stills free space " << (this->nElem - this->index) << endl;
            } else {
                cout << "No free space " << endl;
            }
        }
        // Destructor
        ~Matrix(){
            delete[] this->matrix;
        }
};