/* Templates C4 programa en C++ Define a class called Matrix with data T.

    Menu:
    1. Add a new element if it has space
    2. Check if there is space
    3. Print matrix
    4. Erase matrix
    5. Exit
*/

#include <iostream>
#include "matrix.h"

void menu (int * still);
void showOptions();
void performOptions(int * still);

using namespace std;
int value;
Matrix <int> matrix1(10);

int main(){
    int still = 1;
    

    while (still){
        menu(&still);
    }
    return 0;
}

void menu (int * still) {
    showOptions();
    performOptions(still);
}

void showOptions () {
    cout << "Choose one option please:\n" << \
        "\t 1. Insert a new element into the array\n" <<   \
        "\t 2. Check if there is space on array\n" <<  \
        "\t 3. Print all\n" <<  \
        "\t 4. Delete all\n" <<  \
        "\t 5. Exit\n"  ;
}

void performOptions(int *still) {
    int selector = 0;
    *still = 1;
    cin >> selector;
    switch (selector) {
        case 1:
            if (matrix1.matrixFull()) {
                cout << "Matrix is full please empty if you want to add more vvalues " << endl;
            }else {
                cout << "Insert a value:" << endl;
                cin >> value;
                matrix1.addElem(value);
            }
            break;
        case 2:
            if (matrix1.matrixFull()) {
                cout << "Matrix is full please empty if you want to add more vvalues " << endl;
            }else {
                cout << "There is still space in the matrix " << endl;
                matrix1.getFreeSpace();
            }
            break;
        case 3:
            matrix1.showMatrix();
            break;
        case 4:
            matrix1.emptyMatrix();
            break;
        case 5:
            cout << "Closing environment" << endl;
            *still = 0;
            cout << "Environment closed" << endl;
            break;
        default:
            cout << "Didn't insert a valid value please try again between 1-5" << endl;
            *still = 1;
            break;
    }
}