// Primer programa en C++

#include <iostream>
#include "interchange.h"


using namespace std;

int main(){
    int datoA,datoB;
    cout<< "Introduce el dato 1 ";
    cin >> datoA;
    cout << "Introduce el dato 2 ";
    cin >> datoB;
    interchange(datoA,datoB);
    cout << "Dato 1 ahora es " << datoA << endl;
    cout << "Dato 2 ahora es " << datoB << endl;
    string datoC,datoD;

    cout<< "Introduce el dato 1 ";
    cin >> datoC;
    cout << "Introduce el dato 2 ";
    cin >> datoD;
    interchange(datoC,datoD);
    cout << "Dato 1 ahora es " << datoC << endl;
    cout << "Dato 2 ahora es " << datoD << endl;
    return 0;
}