/* 
    Get the maximum value from array
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;

vector<int> extractIntegerWords(string str,vector<int> vect);

int main(){
	
    
    string temp0;
    vector<int> vector;
	int maxval;

    cout << "Introduce the values separated by spaces:"; getline(cin,temp0);
    //cout << temp0;
    vector = extractIntegerWords(temp0,vector);
    cout << vector.size() << endl;
    for (int i=0;i<vector.size();i++)
    {
        
        if (i==0) 
        {
             maxval = vector[i];
        
        }else 
        {
            if (maxval < vector[i])
            { 
                maxval = vector[i];
            }
        }
    }
    //system("pause");
    cout << "El máximo valor es " << maxval;
	int i = getchar(); //Provioene del stdio.h
    return 0;
}

vector<int> extractIntegerWords(string str,vector<int> vect)
{
    stringstream ss;
    /* Storing the whole string into string stream */
    ss << str;

    /* Running loop till the end of the string */
    string temp;
    int found;
    //cout << ss.str() << endl << endl;
    while (!ss.eof())
    {
        /* Extracting word by word from stream */
        ss >> temp;
        /* Checking the given word is integer or not */
        if (stringstream(temp) >> found)
        {
            vect.push_back(found);
            //cout << found <<endl;
        }
        /* To save from space at the end of string */
        temp = "";
    }
    return vect;
}