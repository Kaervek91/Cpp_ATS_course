/* 
    Define a vector of numbers and calculate the product from all of them
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <math.h>

using namespace std;


int main(){
	
    int number[] = {1,2,3,4,5,6,7,8,9};
	int total = 1;
	cout << "Calculate value of array " << endl;
	cout << sizeof(number)/sizeof(*number)<< endl;
	for (int i=0;i<(sizeof(number)/sizeof(*number));i++)
	{
		cout << "Total = " << total << " * " << number[i] << " = ";
		total*=number[i];
		cout << total << endl;
	};
	cout << "Total value is " << total;
    //system("pause");
	int i = getchar(); //Provioene del stdio.h
    return 0;
}

