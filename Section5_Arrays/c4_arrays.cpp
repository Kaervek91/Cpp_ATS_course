/* 
    Get from the user an array and display it in reverse
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;

vector<int> extractIntegerWords(string str,vector<int> vect);

int main(){
	
    
    string temp0;
    vector<int> vector;
	
    cout << "Introduce the values separated by spaces:"; getline(cin,temp0);
    //cout << temp0;
    vector = extractIntegerWords(temp0,vector);
    for (int i= (vector.size()-1); i>=0;i--)
    {
        cout << "INDEX " << i << " :";
        cout << vector[i] << endl;
    }
    //system("pause");
	int i = getchar(); //Provioene del stdio.h
    return 0;
}

vector<int> extractIntegerWords(string str,vector<int> vect)
{
    stringstream ss;
    /* Storing the whole string into string stream */
    ss << str;

    /* Running loop till the end of the string */
    string temp;
    int found;
    //cout << ss.str() << endl << endl;
    while (!ss.eof())
    {
        /* Extracting word by word from stream */
        ss >> temp;
        /* Checking the given word is integer or not */
        if (stringstream(temp) >> found)
        {
            vect.push_back(found);
            //cout << found <<endl;
        }
        /* To save from space at the end of string */
        temp = "";
    }
    return vect;
}