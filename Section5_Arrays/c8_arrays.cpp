/* 
    Bring one vector pass it to another multiply it by two and show it
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;

vector<int> extractIntegerWords(string str);

int main(){
	
    
    string temp0;
    vector<int> vector1;
    vector<int> vector2;
	int maxval;

    cout << "Introduce the values for first vector separated by spaces:"; getline(cin,temp0);
    //cout << temp0;
    vector1 = extractIntegerWords(temp0);
    for (vector<int>::const_iterator i = vector1.begin(); i != vector1.end(); i++)
    {
        vector2.push_back(*i*2);
    }
    
    
    for (vector<int>::const_iterator i = vector2.begin(); i != vector2.end(); i++)
    {
        cout << *i << " ";
    }
    //system("pause");
	int i = getchar(); //Provioene del stdio.h
    return 0;
}

vector<int> extractIntegerWords(string str)
{
    vector<int> vect;
    stringstream ss;
    /* Storing the whole string into string stream */
    ss << str;

    /* Running loop till the end of the string */
    string temp;
    int found;
    //cout << ss.str() << endl << endl;
    while (!ss.eof())
    {
        /* Extracting word by word from stream */
        ss >> temp;
        /* Checking the given word is integer or not */
        if (stringstream(temp) >> found)
        {
            vect.push_back(found);
            //cout << found <<endl;
        }
        /* To save from space at the end of string */
        temp = "";
    }
    return vect;
}