/* 
    Stacks : Ex2 : Create a program using Stacks which has the next menu
    1. Inserte a character to the stack
    2. Show all elements of the stack
    3. Exit
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

const string names[6] = {"Manuel","Pedro","Hugo","Lorena","Sabela","Isabel"};
const string surnames[6] = {"Mallo","Orihuela","Méndez","Alonso","Montenegro","Saba"};
const string gender[2] = {"Varón","Mujer"};
const string subjects[5]={"Física","Química","Matemáticas","Lengua","Filosofía"};

struct Subject {
    string study;
    float mark;
};

struct Student {
    string name;
    string surname;
    int age;
    string sex;
    Subject * subjects;    
    float avgMark = 0;

} BadgePrototype;

struct Classroom{ 
    Student * collectionStudents;
} Class, * pClass = &Class;

struct Nodo{
    Nodo *next;
    int value;
};

float RandomFloat(float a, float b);
void Calculate_Avg(Student &pStudent);
void CreateSubjects(Student &pStudent);
void CreateClassRoom(Classroom *pClass,int nAlumnos);
void Best(Classroom *pClass,int nAlumnos);
void PrintStudent(Student &pStudent);
int printStack(Nodo *& pila, short next);
void printStackAll(Nodo *&pila);
void addNodeToStack(Nodo *&pila, int n);
void delNodeToStack(Nodo *&pila, int &n);
void menu(short &val);

int main(){
    short exit = 0;
    short selector = 0;
    int number;
    char printAll = 0;
    Nodo *PILA = NULL;
    do{
        exit = 0;
        menu(selector);
        switch(selector){
            case 1:
                cout << "Introduce the number: " ; cin >> number; cout << endl;
                addNodeToStack(PILA,number);
                break;
            case 2:
                if (PILA !=NULL){
                    cout<< "Deleting last number in the stack" << endl;
                    delNodeToStack(PILA,PILA->value);
                    cout << "Deleted" << endl;
                } else {
                    cout << "Nothing to erase" << endl;
                }
                break;
            case 3:
                if (PILA !=NULL){
                    cout << "Printing values" << endl;
                    printStackAll(PILA);
                }else{
                    cout << "No values to print at all" << endl;
                }
                break;
            case 4:
                
                if (PILA != NULL){
                    cout << "Deleting all" << endl;
                    while (PILA->next != NULL) {
                        delNodeToStack(PILA,PILA->value) ;
                    };
                    delNodeToStack(PILA,PILA->value);
                } else{
                    cout << "Nothing to erase" << endl;
                }
                
                break;                
            case 5:
                cout << "Bye bye my friend!" << endl;
                exit = 1;
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
        
    }while (!exit);
    
    
    return 0;

}


void menu(short &val){
    cout << "Choose one option please:\n" << \
        "\t 1. Insert one character to the stack\n" <<   \
        "\t 2. Delete last character from the stack\n" <<  \
        "\t 3. Print all\n" <<  \
        "\t 4. Delete all\n" <<  \
        "\t 5. Exit\n"  ;
        cin >> val;
}
void addNodeToStack(Nodo *&pila, int n){
    Nodo *new_node = new Nodo();
    new_node->value = n;
    new_node->next= pila;
    pila = new_node;
}
void delNodeToStack(Nodo *&pila, int &n){
    Nodo *aux= pila;
    n = aux->value;
    pila = aux->next;
    delete aux;
    
}


int printStack(Nodo *& pila, short next){
    if (next){
        if (pila->next != NULL){
            cout << printStack(pila->next,next) << endl;
        }
    }

    return pila->value;
    
}

void printStackAll(Nodo *&pila){
    
    cout << printStack(pila,1) << endl;
}



void CreateClassRoom(Classroom * pClass,int nAlumnos){
    pClass->collectionStudents = new Student [nAlumnos];
    for (int i = 0 ; i<nAlumnos; i++){
        int nameN =  (rand() % 6);
        pClass->collectionStudents[i].name = names[nameN];
        pClass->collectionStudents[i].surname = surnames[(rand() % 6)];
        pClass->collectionStudents[i].sex =  (nameN <= 3) ? gender[0] : gender[1];
        pClass->collectionStudents[i].age = rand() % 20;
        CreateSubjects( pClass->collectionStudents[i]);
    }
}

void CreateSubjects(Student &pStudent){

    //cout << "TAMAÑO SUBJECTS " << sizeof(subjects)/sizeof(string) << endl;
    pStudent.subjects = new Subject [sizeof(subjects)/sizeof(string)];
    /* Autocompletar asignaturas*/
    for (int j = 0 ; j < sizeof(subjects)/sizeof(string); j++){
        //cout << subjects[j]<< endl;
        pStudent.subjects[j].study = subjects[j];
        pStudent.subjects[j].mark = RandomFloat(0,10);
    }
    Calculate_Avg(pStudent);

}

void PrintStudent(Student &pStudent){
    cout << "  ---------------------------- Student ----------------------------" << endl;
        cout << "  Name and surname " << pStudent.name << " " << pStudent.surname << endl; 
        cout << "  Age " << pStudent.age << endl;
        cout << "  Country " << pStudent.sex << endl;
        cout << "  Average " << pStudent.avgMark << endl;
        
        for (int j = 0; j < (sizeof(subjects)/sizeof(string));j++){
            cout << "  \t Subject " << pStudent.subjects[j].study << " : " << pStudent.subjects[j].mark << endl;
        }
        cout << "  ---------------------------- Student end ------------------------" << endl;
}

void Calculate_Avg(Student &pStudent){
    for (int j = 0; j < (sizeof(subjects)/sizeof(string));j++){
            pStudent.avgMark += pStudent.subjects[j].mark;
        }
    pStudent.avgMark = pStudent.avgMark / (sizeof(subjects)/sizeof(string));
};

void Best(Classroom *pClass,int nAlumnos){
    Student * pBest = NULL;
    float avgMark = 0.f;
    for (int i = 0 ; i<nAlumnos; i++){
        if (i== 0){
            avgMark = pClass->collectionStudents[i].avgMark;
            pBest =  &pClass->collectionStudents[i];
        } else if(avgMark < pClass->collectionStudents[i].avgMark){
            avgMark = pClass->collectionStudents[i].avgMark;
            pBest =  &pClass->collectionStudents[i];
        }    
    }
    cout << "Best Student" << endl;
    PrintStudent(*pBest);
}

float RandomFloat(float a, float b){
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

int ** BuildShowMatrix(int ** matrix,int Rows, int Cols){
    matrix = new int * [Rows];
    for (int i=0; i<Rows; i++){
        matrix[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(matrix + i)+j) = rand() % 10;
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            cout << matrix[i][j] << " " ;
        }    
        cout << endl;
        
    }
    return matrix;
}

int ** Traspuesta(int **matrix,int Rows, int Cols){
    int **traspuesta = new int * [Rows];
    
    for (int i=0; i<Rows; i++){
        traspuesta[i] = new int [Cols]; 
    }
    cout << "Pay9o9o" << endl;
    for (int i=0 ;i < Rows; i++){
        for (int j=0;j<Cols;j++){
            
            *(*(traspuesta+j)+i) = *(*(matrix + i)+j);
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            //cout << matrix[i][j] << " " ;
            cout << traspuesta[i][j] << " ";
        }   
        cout << endl;
    }
    return traspuesta;
}

void FreeMemory(int ** matrix,int Rows, int Cols){
    
    for (int i=0; i<Rows; i++){
        delete[] *(matrix + i);
        //cout << i<< endl;
    }
    delete[] matrix;
}

int  AnalyzeArray(char * array, int * length, int * vowelP){
    int * result = new int;

    for (int i = 0 ; i < *length; i++) {
        //cout << *(array+i) << endl;
        switch (*(array + i)){
            case 'a':
                (*result)++;
                (*vowelP)++;
                break;
            case 'e':
                (*result)++;
                (*(vowelP+1))++;
                break;
            case 'i':
                (*result)++;
                (*(vowelP+2))++;
                break;
            case 'o':
                (*result)++;
                (*(vowelP+3))++;
                break;
            case 'u':
                (*result)++;
                (*(vowelP+ 4))++;
                break;
            default:
                break;
        }
        
    }
    cout << result << " " << *result << endl;
    
    return *result;
}


int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}


void intercambio(int &a, int &b){
    int aux;
    aux = a;
    a = b;
    b = aux;
}

void search(int a[],int number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}
void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

