/* 
    Queues : Ex1-2 : Create a bank program using LISTS which stores the values of clients from a bank and then show the clients in correct order.
    1. Inserte a character to the stack
    2. Show all elements of the stack
    3. Exit
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

const string names[6] = {"Manuel","Pedro","Hugo","Lorena","Sabela","Isabel"};
const string surnames[6] = {"Mallo","Orihuela","Méndez","Alonso","Montenegro","Saba"};
const string gender[2] = {"Varón","Mujer"};
const string subjects[5]={"Física","Química","Matemáticas","Lengua","Filosofía"};

struct Account{
    double id;
    float money;
};

struct User{
    string name;
    string surname;
    int age;
    string sex;
    Account account;
    int order; 
};

struct Nodo{
    Nodo *next;
    User user;
};

void searchElement(Nodo *list, double n);
void showElement(Nodo *list);
void showListElements(Nodo *list);
void delNodeFromList(Nodo *&list, double &n);
void addNode2List(Nodo *&list,User n);
int numberOfClients(Nodo *&list);
double totalMoneyBank(Nodo *list);
User richest(Nodo *list);

void menu(short &val);
float RandomFloat(float a, float b);
void printUser(User user);
void CreateUser(User &User);

int main(){
    short exit = 0;
    short selector = 0;
    User GeneralUser;
    char printAll = 0;
    Nodo *list = NULL;

    do{
        exit = 0;
        menu(selector);
        switch(selector){
            case 1:
                cout << "Introduce the User: " ;
                CreateUser(GeneralUser);
                addNode2List(list, GeneralUser);
                break;
            case 2:
                if (list != NULL){
                    double id;
                    cout<< "Deleting User from ID number entered " << endl;
                    cin>> id;
                    delNodeFromList(list,id);
                    cout << "Deleted" << endl;
                } else {
                    cout << "Nothing to erase" << endl;
                }
                break;
            case 3:
                if (list != NULL){
                    cout << "Printing values" << endl;
                    showListElements(list);
                    
                }else{
                    cout << "No values to print at all" << endl;
                }
                break;
            case 4:
                
                if (list != NULL){
                    cout << "Deleting all" << endl;
                    Nodo *actual = new Nodo();
                    Nodo * aux ;
                    actual = list;
                    while (actual != NULL) {
                        cout << "Deleting " << actual->user.name << " " << actual->user.surname  << endl;
                        aux = actual->next;
                        delNodeFromList(actual,actual->user.account.id) ;
                        actual = aux;
                    };
                    
                } else{
                    cout << "Nothing to erase" << endl;
                }
                break;                
            case 5:
                cout << "Looking for the richest client!" << endl;
                printUser(richest(list));
                break;
            case 6:
                cout << "Total money in bank is " << endl;
                cout << totalMoneyBank(list);
                break;
            case 7:
                cout << "Total number of clients!" << endl;
                cout << numberOfClients(list);
                break;
            case 8:
                cout << "Bye bye my friend!" << endl;
                exit = 1;
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
        
    }while (!exit);
    
    
    return 0;

}


void menu(short &val){
    cout << "Choose one option please:\n" << \
        "\t 1. Insert one character to the stack\n" <<   \
        "\t 2. Delete last character from the stack\n" <<  \
        "\t 3. Print all\n" <<  \
        "\t 4. Delete all\n" <<  \
        "\t 5. Richest Client\n" <<  \
        "\t 6. Total Money\n" <<  \
        "\t 7. Number of Clients\n" <<  \
        "\t 8. Exit\n"  ;
        cin >> val;
}

void addNode2List(Nodo *&list,User n){
    Nodo *new_node = new Nodo();
    new_node->user = n;
    Nodo *aux1 = list;
    Nodo *aux2;
    while((aux1 != NULL) && (aux1->user.account.id < n.account.id)){
        aux2 = aux1;
        aux1 = aux1->next;
    }
    
    if (aux1 == list){
        list = new_node;
    } else {
        aux2->next = new_node;
    }
    new_node->next = aux1;
}

void delNodeFromList(Nodo *&list, double &n){
    if(list != NULL) {
        Nodo *aux_borrar;
        Nodo *anterior =NULL;
        aux_borrar = list;
        while ( (aux_borrar != NULL) && (aux_borrar->user.account.id != n)){
            anterior = aux_borrar;
            aux_borrar = aux_borrar->next;
        }

        if(aux_borrar == NULL){
            cout << "Elemento inexistente" << endl;
        } else if( anterior == NULL){
            list = list->next;
            delete aux_borrar;
        } else {
            anterior->next = aux_borrar->next;
            delete aux_borrar;
        } 
    } else {
            cout << "Empty list " << endl;
    }
    
}

void showListElements(Nodo *list){
    Nodo *actual =new Nodo();
    actual = list;
    while (actual != NULL){
        printUser(actual->user);
        actual = actual->next;
    }  
    
}

void showElement(Nodo *list){
        
    printUser(list->user) ;
    
}

void searchElement(Nodo *list, double n){
    Nodo *actual = new Nodo();
    bool band = false;
    while ( (actual != NULL ) && (actual->user.account.id <=n)){
        if (actual->user.account.id == n ){
            band = true;
        } else {
            actual = actual->next;
        }
        
    }
    if (band){
        showElement(actual);
    }else {
        cout << "Element ID not found -> "<< n << endl;
    }
}

void CreateUser(User &User){
    int nameN =  (rand() % 6);
    User.name = names[nameN];
    User.surname = surnames[(rand() % 6)];
    User.sex = (nameN <= 3) ? gender[0] : gender[1];
    User.age = rand() % 80;
    User.account.id = rand() % 9999999;
    User.account.money = RandomFloat(0,9999999);
}

float RandomFloat(float a, float b){
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void printUser(User user){
    cout << "\n\t User data \n";
    cout << user.name << endl;
    cout << user.surname << endl;
    cout << user.sex << endl;
    cout << user.age << endl;
    cout << fixed << user.account.id << endl;
    cout << fixed << user.account.money << endl;
}

double totalMoneyBank(Nodo *list){
    Nodo *actual = new Nodo();
    actual = list;
    double money;
    while (actual != NULL){
        money += actual->user.account.money;
        actual = actual->next;
    }
    return money;
}

User richest(Nodo *list){
    User richest;
    richest.account.money = 0;
    Nodo *actual = new Nodo();
    actual = list;
    while (actual != NULL){
        if (actual->user.account.money > richest.account.money){
            richest = actual->user;
        }
        actual = actual->next;
    }
    return richest;
}

int numberOfClients(Nodo *&list){
    Nodo *actual = new Nodo();
    actual = list;
    int numberClients = 0;
    while (actual != NULL){
        
        numberClients++;
        
        actual = actual->next;
    }
    numberClients--;
    return numberClients;
}