/* 
    Método de ordenación de un arreglo con el método burbuja
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(int number[]);

int main()
{
    string chain;
    
    int number[5] = {3,2,5,11,1};
    int i,k,aux;
    // ALGORITMO DEL MÉTODO BURBUJA
    for (i=0;i<5;i++){
        for (k=0; k<5; k++){
            if (number[i]< number[k]){ //faster
            //if (number[k]> number[k+1]){ //slower
                aux = number[i]; //faster
                //aux = number[k]; //slower
                number[i] = number[k]; //faster
                //number[k] = number[k+1]; //slower
                number[k] = aux; //faster
                //number[k+1] = aux; //slower
                print(number);
            }
            
        }
        //print(number);
    }
    //print(number);
    return 0;

}

void print (int number[]){   
    int i;
    for (i=0;i<5; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}