/* 
    Método de ordenación de un arreglo con el método quickshort
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(float number[], int n);
void quickShort(float a[],int first, int last);


int main(){
    string chain;
    
    float number[8] = {5,2,3,6,1,8,7,0};
    
    
    quickShort(number,0,7);
    print (number,7);
    //print(number);
    return 0;

}

void print (float number[], int n){   
    int i;
    for (i=0;i<n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void quickShort(float a[],int first, int last){
    int middle,i,j;
    float pivot;
    middle = (first + last )/2;
    pivot = a[middle];
    i = first;
    j = last;

    do{
        while(a[i]< pivot) i++;
        while(a[j]> pivot) j--;

        if (i<=j){
            intercambio(a[i], a[j]);
            i++;
            j--;
        }
    }while(i<=j);
    
    if (first < j){
        quickShort(a,first,j);
    }

    if (i < last){
        quickShort(a,i,last);
    }

}