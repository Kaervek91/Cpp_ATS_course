/* 
    Método de ordenación de un arreglo con el método inserción decreciente o método shell
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(int number[], int n);
void ordenacionShell(float a[],int w);


int main(){
    string chain;
    
    int number[8] = {5,2,3,6,1,8,7,0};
    int i,pos,min,aux;
    int n = 8;
    // ALGORITMO DEL  método inserción decreciente o método shell
    for (i=0;i<n;i++){
        min = i;
        for (int k=i+1;k<n;k++){
            if ( number[k] < number[min]) {
                min = k;
            }
            //print(number);
        }
        aux = number[i];
        number[i] = number[min];
        number[min] = aux;
        
        print(number,n);
            
        //print(number);
    }
    //print(number);
    return 0;

}

void print (int number[], int n){   
    int i;
    for (i=0;i<n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void ordenacionShell(float a[],int w){
    int salto, i,k,j;
    salto = w/2;

    while(salto >0){
        for (i=salto;i<w;i++){
        j = i - salto;
        while(j>=0){
            k = j+salto;
            if(a[j] <= a[k]){ //Par de elementos ordenados
                j=-1;
            } else { //Par de elementos desornedados
                intercambio(a[j],a[k]);
                j -=salto;
            }
        }
        salto = salto/2;
        }
    }


}