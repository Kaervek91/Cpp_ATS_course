/* 
    Método de ordenación de un arreglo con el método seleccion
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(int number[]);

int main()
{
    string chain;
    
    int number[5] = {5,2,3,6,1};
    int i,pos,min,aux;
    // ALGORITMO DEL metodo seleccion
    for (i=0;i<5;i++){
        min = i;
        for (int k=i+1;k<5;k++){
            if ( number[k] < number[min]) {
                min = k;
            }
            //print(number);
        }
        aux = number[i];
        number[i] = number[min];
        number[min] = aux;
        
        print(number);
            
        //print(number);
    }
    //print(number);
    return 0;

}

void print (int number[]){   
    int i;
    for (i=0;i<5; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}