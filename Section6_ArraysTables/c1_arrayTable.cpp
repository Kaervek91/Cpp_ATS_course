/* 
    Get the a value from a avector which the sum of the rest represents this value
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <typeinfo>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;

int randomValue();

int main()
{
	
    
    int temp0, temp1;
    char stay = 1;

    while (stay) 
    { 
        cout << "Introduce the number of rows:"; cin >> temp0; cout << endl;
        if (typeid(temp0) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }
    stay = 1;
    while (stay) 
    { 
        cout << "Introduce the number of cols:"; cin >> temp1; cout << endl;
        if (typeid(temp0) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }

    int array[temp0][temp1];

    for (int i=0;i<temp0;i++)
    {
        for (int j = 0; j<temp1;j++)
        {
            array[i][j] = rand() % 100 + 1;
        }
    }
    cout << "Show result {" ;
    for (int i=0;i<temp0;i++)
    {
        cout << "{";
        for (int j = 0; j<temp1;j++)
        {
            cout << array[i][j];
            if ((j+1)  < (temp1)) cout << ",";
        }
        cout << "}";
        if ((i+1)<(temp0)) 
        {
            cout << ",";
        }else {
            cout << "}" << endl;
        }
        
    }
    

}
