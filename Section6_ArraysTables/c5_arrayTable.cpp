/* 
    Crear una matriz transpuesta
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <typeinfo>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;


void GetValues(int *rows,int *cols );
void AddValues(int **array,int *rows,int *cols);
void PrintArray(int **array,int *rows,int *cols);
void PrintDiagonal(int **array,int *rows,int *cols);
void CopyArray(int **arrayDest,int **arraySrc,int *rows,int *cols);
void TranspArray(int **arrayDest,int **arraySrc,int *rows,int *cols);

int main()
{
	
    int rows, cols;
    GetValues(&rows,&cols);
    int **arrayP = new int*[rows];
    for(int i= 0; i< rows; i++) arrayP[i] = new int[cols];
    int **arrayP2 = new int*[rows];
    for(int i= 0; i< rows; i++) arrayP2[i] = new int[cols];
    AddValues(arrayP,&rows,&cols);
    cout << "arrayP" << endl;
    PrintArray(arrayP,&rows,&cols);
    cout<< endl;
    //PrintDiagonal(arrayP,&rows,&cols);
    TranspArray(arrayP2,arrayP,&rows,&cols);
    //GenerateArray(&array,&rows, &cols);
    cout << "arrayP2" << endl;
    PrintArray(arrayP2,&rows,&cols);
    
    

}

void GetValues(int *rows,int *cols ){

    char stay = 1;

    while (stay) 
    { 
        cout << "Introduce the number of rows:"; cin >> *rows; cout << endl;
        if (typeid(*rows) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }
    stay = 1;
    while (stay) 
    { 
        cout << "Introduce the number of cols:"; cin >> *cols; cout << endl;
        if (typeid(*cols) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }

}

void AddValues(int **arrayP,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayP[i][j] = rand() % 100 + 1;
        }
    }
}

void PrintArray(int **array, int *rows,int *cols){
    /*cout  << "Rows " << sizeof(*array)/sizeof(**array) << endl;
    int rows = sizeof(array)/sizeof(array[0]);
    cout << rows << endl;
    int cols  = sizeof(array[0])/sizeof(array[0][0]);;
    cout << cols << endl;
    */
    cout << "Show result {" ;
    for (int i=0;i<*rows;i++)
    {
        cout << "{";
        for (int j = 0; j<*cols;j++)
        {
            cout << array[i][j];
            if ((j+1)  < (*cols)) cout << ",";
        }
        cout << "}";
        if ((i+1)<(*rows)) 
        {
            cout << ",";
        }else {
            cout << "}" << endl;
        }
        
    }
}

void PrintDiagonal(int **array, int *rows,int *cols){
    /*cout  << "Rows " << sizeof(*array)/sizeof(**array) << endl;
    int rows = sizeof(array)/sizeof(array[0]);
    //cout << rows << endl;
    int cols  = sizeof(array[0])/sizeof(array[0][0]);;
    cout << cols << endl;*/
    cout << "Show result {" ;
    for (int i=0;i<*rows;i++)
    {
        //cout << "{";
        for (int j = 0; j<*cols;j++)
        {
            if (i==j) {
                cout << array[i][j];
            //if ((j+1)  < (cols)) cout << ",";
            }
        }
        //cout << "}";
        if ((i+1)<(*rows)) 
        {
            cout << ",";
        }else {
            cout << "}" << endl;
        }
        
    }


}

void CopyArray(int **arrayDest,int **arraySrc,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayDest[i][j] = arraySrc[i][j];
        }
    }
}

void TranspArray(int **arrayDest,int **arraySrc,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayDest[i][j] = arraySrc[j][i];
        }
    }
}