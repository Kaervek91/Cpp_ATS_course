/* 
    Crear una matrices y determinar si es simetrica o no, una matriz es simetrica si es cuadrada y si es igual a su matriz transpuesta
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include <iostream>
#include <typeinfo>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <vector>
#include <math.h>

using namespace std;


void GetValues(int *rows,int *cols );
void AddValues(int **array,int *rows,int *cols);
void PrintArray(int **array,int *rows,int *cols);
void PrintDiagonal(int **array,int *rows,int *cols);
void CopyArray(int **arrayDest,int **arraySrc,int *rows,int *cols);
void TranspArray(int **arrayDest,int **arraySrc,int *rows,int *cols);
void SumArray (int **arrayDest,int **arraySrc,int *rows,int *cols);
bool SimetricArray(int **arrayDest,int **arraySrc,int *rows,int *cols);
void AddSimetricVals(int **array,int *rows,int *cols);
void MultiplyArrays(int **arrayFinal,int **array1,int **array2,int *rows,int *cols ,int *rows2,int *cols2);


int main()
{
	
    int rows, cols, rows2,cols2;
    cout << "Introduce M1" << endl;
    GetValues(&rows,&cols);
    cout << "Introduce M2" << endl;
    GetValues(&rows2,&cols2);
    
    if (rows==cols2){
        int **arrayP = new int*[rows];
        for(int i= 0; i< rows; i++) arrayP[i] = new int[cols];
        int **arrayP2 = new int*[rows2];
        for(int i= 0; i< rows; i++) arrayP2[i] = new int[cols2];
        int **arrayP3 = new int*[rows];
        for(int i= 0; i< rows; i++) arrayP3[i] = new int[cols2];
        AddValues(arrayP,&rows,&cols);
        AddValues(arrayP2,&rows2,&cols2);
        cout << "arrayP" << endl;
        PrintArray(arrayP,&rows,&cols);
        cout << "arrayP2" << endl;
        PrintArray(arrayP2,&rows2,&cols2);
        cout<< endl;
        MultiplyArrays(arrayP3,arrayP,arrayP2,&rows,&cols,&rows2,&cols2);
        PrintArray(arrayP3,&rows,&cols2);
    
    } else {
        cout << "Both arrays can't be multiplied" << endl;
    }
    
    
    
    
    

}

void GetValues(int *rows,int *cols ){

    char stay = 1;

    while (stay) 
    { 
        cout << "Introduce the number of rows:"; cin >> *rows; cout << endl;
        if (typeid(*rows) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }
    stay = 1;
    while (stay) 
    { 
        cout << "Introduce the number of cols:"; cin >> *cols; cout << endl;
        if (typeid(*cols) == typeid(int)){
            stay = 0;
        } else 
        {
            cout << "Not a number please, ";
        }
    }

}

void AddValues(int **arrayP,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayP[i][j] = rand() % 100 + 1;
        }
    }
}

void PrintArray(int **array, int *rows,int *cols){
    /*cout  << "Rows " << sizeof(*array)/sizeof(**array) << endl;
    int rows = sizeof(array)/sizeof(array[0]);
    cout << rows << endl;
    int cols  = sizeof(array[0])/sizeof(array[0][0]);;
    cout << cols << endl;
    */
    //cout << *rows << " and " << *cols;
    cout << "Show result {" ;
    for (int i=0;i<*rows;i++)
    {
        cout << "{";
        for (int j = 0; j<*cols;j++)
        {
            cout << array[i][j];
            if ((j+1)  < (*cols)) cout << ",";
        }
        cout << "}";
        if ((i+1)<(*rows)) 
        {
            cout << ",";
        }else {
            cout << "}" << endl;
        }
        
    }
}

void PrintDiagonal(int **array, int *rows,int *cols){
    /*cout  << "Rows " << sizeof(*array)/sizeof(**array) << endl;
    int rows = sizeof(array)/sizeof(array[0]);
    //cout << rows << endl;
    int cols  = sizeof(array[0])/sizeof(array[0][0]);;
    cout << cols << endl;*/
    cout << "Show result {" ;
    for (int i=0;i<*rows;i++)
    {
        //cout << "{";
        for (int j = 0; j<*cols;j++)
        {
            if (i==j) {
                cout << array[i][j];
            //if ((j+1)  < (cols)) cout << ",";
            }
        }
        //cout << "}";
        if ((i+1)<(*rows)) 
        {
            cout << ",";
        }else {
            cout << "}" << endl;
        }
        
    }


}

void CopyArray(int **arrayDest,int **arraySrc,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayDest[i][j] = arraySrc[i][j];
        }
    }
}

void TranspArray(int **arrayDest,int **arraySrc,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayDest[i][j] = arraySrc[j][i];
        }
    }
}

void SumArray (int **arrayDest,int **arraySrc,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            arrayDest[i][j] += arraySrc[i][j];
        }
    }
}

bool SimetricArray(int **arrayDest,int **arraySrc,int *rows,int *cols){
    bool SimArr = true;
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            if( arrayDest[i][j] != arraySrc[i][j]){
                SimArr = false;
            }
        }
    }
    return SimArr;
}

void AddSimetricVals(int **array,int *rows,int *cols){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols;j++)
        {
            array[i][j] = rand() % 100 + 1;
            array[j][i] = array[i][j];
        }
    }
}

void MultiplyArrays(int **arrayFinal,int **array1,int **array2,int *rows,int *cols ,int *rows2,int *cols2){
    for (int i=0; i<*rows;i++)
    {
        for (int j = 0; j<*cols2;j++)
        {
            arrayFinal[i][j] = 0;
            for(int k=0;k<*rows;k++){
                arrayFinal[i][j]=arrayFinal[i][j] + (array1[i][k]*array2[k][j]);
            }
        }
    }
}