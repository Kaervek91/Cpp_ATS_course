/* 
    Pointers Exercise 17 Pointers to structs. Make an struct called Cyclist with several stages with fields like hours, minutes, seconds and so then
    check which was the best stage due to the avgSpeed and print out use pointers!!
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

struct Stage {
    int stageID;
    float distanceKM;
    int hours;
    int minutes;
    int seconds;
    float avgSpeed;
};

struct Cyclist {
    string name;
    int age;
    Stage bestStage;
    Stage * collectionStages;

} Cyclist, *CyclistP = &Cyclist;

int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols);
void FreeMemory(int ** matrix,int Rows, int Cols);
int ** BuildShowMatrix(int ** matrix,int Rows, int Cols);
int ** Traspuesta(int ** matrix,int Rows, int Cols);
void PrintArray(int * array, int * length);
int   AnalyzeArray(char * array, int * length, int * vowelP);
int *  OrderArray(int * array, int * length);
int * FindNumber(int number,int * array, int * length);
void search(int a[],int number,int first, int last);


int main(){
    int numberOfStages = 0;
    cout << "Introduce el nombre del ciclista a registrar " ; cin >> CyclistP->name;
    CyclistP->age = rand() % 50;
    cout << "Introduce el número de etapas a registrar " ; cin >> numberOfStages;
    CyclistP->collectionStages = new struct Stage [numberOfStages];

    for (int i = 0; i< numberOfStages;i++){
        string aux;
        fflush(stdin);
        cout << "  ---------------------------- Stage number " << (i+1) << " ----------------------------" << endl;
        CyclistP->collectionStages[i].stageID = i+1; // Distance is in KM
        CyclistP->collectionStages[i].distanceKM = rand() % 300; // Distance is in KM
        CyclistP->collectionStages[i].avgSpeed = rand() % 30 + 20; //Speed is in KM/H
        float timeTaken = (CyclistP->collectionStages[i].distanceKM)/(CyclistP->collectionStages[i].avgSpeed);
        CyclistP->collectionStages[i].hours = timeTaken;
        CyclistP->collectionStages[i].minutes = fmod(timeTaken,1) *60.f;
        CyclistP->collectionStages[i].seconds = (fmod(timeTaken,1)*3600.f - CyclistP->collectionStages[i].minutes*60.f);
        cout << "Distance " << CyclistP->collectionStages[i].distanceKM << endl; 
        cout << "Avg Speed " << CyclistP->collectionStages[i].avgSpeed << endl;
        cout << "Time taken " << timeTaken  << endl;
        cout << "Hours " << CyclistP->collectionStages[i].hours << endl;
        cout << "Minutes " << CyclistP->collectionStages[i].minutes << endl;
        cout << "Seconds " << CyclistP->collectionStages[i].seconds << endl;
        cout << "  ---------------------------- Stage number end " << (i+1) << " ------------------------" << endl;
    }
    float aux = 0;
    for (int i = 0; i< numberOfStages;i++){
        if(CyclistP->collectionStages[i].avgSpeed > aux ){
            CyclistP->bestStage = CyclistP->collectionStages[i];
            aux = CyclistP->collectionStages[i].avgSpeed;
        }
      
    }
     cout << "La mejor etapa de " << CyclistP->name << endl;
        cout << "Stage Number " << CyclistP->bestStage.stageID << endl ;
        cout << "Distance " << CyclistP->bestStage.distanceKM << endl ; 
        cout << "Avg Speed " << CyclistP->bestStage.avgSpeed << endl;
        cout << "Hours " << CyclistP->bestStage.hours << endl;
        cout << "Minutes " << CyclistP->bestStage.minutes << endl;
        cout << "Seconds " << CyclistP->bestStage.seconds << endl;
    return 0;

}


int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols){
    int **result = new int * [Rows];
    for (int i=0; i<Rows; i++){
        result[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(result+i)+j) = *(*(matrix + i)+j) + *(*(matrix2 + i)+j);
        }
    }

    return result;
}

int ** BuildShowMatrix(int ** matrix,int Rows, int Cols){
    matrix = new int * [Rows];
    for (int i=0; i<Rows; i++){
        matrix[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(matrix + i)+j) = rand() % 10;
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            cout << matrix[i][j] << " " ;
        }    
        cout << endl;
        
    }
    return matrix;
}

int ** Traspuesta(int **matrix,int Rows, int Cols){
    int **traspuesta = new int * [Rows];
    
    for (int i=0; i<Rows; i++){
        traspuesta[i] = new int [Cols]; 
    }
    cout << "Pay9o9o" << endl;
    for (int i=0 ;i < Rows; i++){
        for (int j=0;j<Cols;j++){
            
            *(*(traspuesta+j)+i) = *(*(matrix + i)+j);
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            //cout << matrix[i][j] << " " ;
            cout << traspuesta[i][j] << " ";
        }   
        cout << endl;
    }
    return traspuesta;
}

void FreeMemory(int ** matrix,int Rows, int Cols){
    
    for (int i=0; i<Rows; i++){
        delete[] *(matrix + i);
        //cout << i<< endl;
    }
    delete[] matrix;
}

int  AnalyzeArray(char * array, int * length, int * vowelP){
    int * result = new int;

    for (int i = 0 ; i < *length; i++) {
        //cout << *(array+i) << endl;
        switch (*(array + i)){
            case 'a':
                (*result)++;
                (*vowelP)++;
                break;
            case 'e':
                (*result)++;
                (*(vowelP+1))++;
                break;
            case 'i':
                (*result)++;
                (*(vowelP+2))++;
                break;
            case 'o':
                (*result)++;
                (*(vowelP+3))++;
                break;
            case 'u':
                (*result)++;
                (*(vowelP+ 4))++;
                break;
            default:
                break;
        }
        
    }
    cout << result << " " << *result << endl;
    
    return *result;
}


int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}


void intercambio(int &a, int &b){
    int aux;
    aux = a;
    a = b;
    b = aux;
}

void search(int a[],int number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}
void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

