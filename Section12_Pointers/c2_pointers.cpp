/* 
    Pointers Número par o impar Ejercicio 2
    Comprobar si un número es par o impar, y señalar la posición de memoria
    donde se está guardando el número.
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>

using namespace std;
void printVector(vector<int> const &vect);


int main(){

    int num;
    vector <int> divisores;
    cout << "Introduzca un número: "; cin >> num;
    int *dir_num = &num;
    int flag = 0;
    for (int i = 0; i<*dir_num; i++){
        if (i != 0){
            if ((*dir_num % i )==0) {
            flag++;
            divisores.push_back(i);
            }

        }
        
    }
    if (flag > 1) {
        cout << "El número " << *dir_num << " no es primo y se encuentra en la dirección de memoria " << dir_num << endl;
    }
    else {
        cout << "El número " << *dir_num << " es primo y se encuentra en la dirección de memoria " << dir_num << endl;   
    }
        
    printVector(divisores);

    return 0;

}

void printVector(vector<int> const &vect){
    cout << "Es divisible por ";

    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i) << ", ";
    }
    cout << "y por si mismo";
    cout << endl;
}