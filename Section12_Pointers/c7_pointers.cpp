/* 
    Pointers Exercise 7  Arrays Transmission
    Exchange the value of 2 variables
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;
void FindMax(float *, int length);

int main(){
    float array[6] = {1,4,5,7,9,6};
    FindMax(array,6);
    
    return 0;

}

void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

