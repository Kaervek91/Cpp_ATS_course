/* 
    Pointers Número par o impar Ejercicio 1
    Comprobar si un número es par o impar, y señalar la posición de memoria
    donde se está guardando el número.
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>


using namespace std;



int main(){

    int num;
    cout << "Introduzca un número: "; cin >> num;
    int *dir_num = &num;
    
    if ( (*dir_num % 2) == 0) {
        cout << "El número " << *dir_num << " es par y se encuentra en la dirección de memoria " << dir_num << endl;
    } else {
        cout << "El número " << *dir_num << " es impar y se encuentra en la dirección de memoria " << dir_num << endl;
    }
    
    
    return 0;

}

