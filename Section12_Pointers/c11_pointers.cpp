/* 
    Pointers Exercise 11 Request the name of the user and return the number of vowels
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
//#include <vector>
#include <math.h>
//#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

char userName[30];
int counter[5] {0,0,0,0,0};
int *vowelP = counter;
int * length = new int;
int * result = new int;

void PrintArray(int * array, int * length);
int   AnalyzeArray(char * array, int * length, int * vowelP);
int *  OrderArray(int * array, int * length);
int * FindNumber(int number,int * array, int * length);
void search(int a[],int number,int first, int last);



int main(){
    *length = 30;
    cout << "Introduzca su nombre " ; cin >> userName;
    cout << userName << endl;
    *result = AnalyzeArray(userName,length , vowelP);
    cout << "Number of vowels " << *result << endl;
    //FindMax(array,6);
    cout << "Vowel A " << *vowelP << endl; 
    cout << "Vowel E " << *(vowelP+1) << endl;
    cout << "Vowel I " << *(vowelP+2) << endl;
    cout << "Vowel O " << *(vowelP+3) << endl;
    cout << "Vowel U " << *(vowelP+4) << endl;
    
    return 0;

}

int  AnalyzeArray(char * array, int * length, int * vowelP){
    int * result = new int;

    for (int i = 0 ; i < *length; i++) {
        //cout << *(array+i) << endl;
        switch (*(array + i)){
            case 'a':
                (*result)++;
                (*vowelP)++;
                break;
            case 'e':
                (*result)++;
                (*(vowelP+1))++;
                break;
            case 'i':
                (*result)++;
                (*(vowelP+2))++;
                break;
            case 'o':
                (*result)++;
                (*(vowelP+3))++;
                break;
            case 'u':
                (*result)++;
                (*(vowelP+ 4))++;
                break;
            default:
                break;
        }
        
    }
    cout << result << " " << *result << endl;
    
    return *result;
}


int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}


void intercambio(int &a, int &b){
    int aux;
    aux = a;
    a = b;
    b = aux;
}

void search(int a[],int number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}
void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

