/* 
    Pointers Exercise 4 Fill an array with n numbers, later find the lower
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>
#include <math.h>


using namespace std;
void printVector(vector<int> const &vect);


int main(){

    int num[10];
    int * min = num, *current = num;
    for (int i=0;i<10;i++){
        num[i]= rand() % 100;
    }
    
    for (int i = 0; i<(sizeof(num)/sizeof(int)); i++){
        cout << num[i]<< endl;;
        
        if (*current <  *min) {
            min = current;
        }
        *current++;
        
    }
    cout << "El número " << *min << " es el mínimo del array y se encuentra en la dirección de memoria " << min << endl;

    return 0;

}

void printVector(vector<int> const &vect){
    cout << "Es divisible por ";

    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i) << ", ";
    }
    cout << "y por si mismo";
    cout << endl;
}