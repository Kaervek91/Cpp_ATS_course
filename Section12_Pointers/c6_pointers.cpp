/* 
    Pointers Exercise 6  Addresses Transmission
    Exchange the value of 2 variables
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;
void exchange(float *, float *);

int main(){
    float a = 123.321;
    float b = 321.123;
    
    cout << "Primer número " << a  << endl; 
    cout << "Segundo número " << b  << endl; 
    cout << "Realizar intercambio!!!!" << endl;
    exchange(&a,&b);
    
    cout << "Primer número " << a  << endl; 
    cout << "Segundo número " << b  << endl; 
    
    return 0;

}

void exchange(float *dir1,float *dir2){
    float aux;
    aux = *dir1;
    *dir1 = *dir2;
    *dir2 = aux;
}
