/* 
    Pointers Exercise 5  Dynamic array assignment
    new : Allocate the number of bytes requested
    delete: Free a bytes block allocated previously
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;
void printVector(vector<int> const &vect);
void RequestMarks();
void ShowMarks();

int numCalif, *Calif;

int main(){

    RequestMarks();
    ShowMarks();
    delete[] Calif; // Liberando espacio en BYtes usados anteriormente
    return 0;

}

void RequestMarks(){
    cout << "Insert number of marks: " ; cin >> numCalif; 
    Calif = new int[numCalif]; // Crear el arreglo
    for (int i=0;i<numCalif;i++){
        Calif[i] = rand() % 100;
    }
}

void ShowMarks(){
    cout << "Mostrar notas del usuario" << endl;
        for (int i=0;i<numCalif;i++){
            cout << Calif[i] << endl;
        }

}


void printVector(vector<int> const &vect){
    cout << "Es divisible por ";

    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i) << ", ";
    }
    cout << "y por si mismo";
    cout << endl;
}