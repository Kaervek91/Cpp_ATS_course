/* 
    Pointers Exercise 8  Request the user N numbers, store them in a dynamic array
    then order those numbers in ascendant and show them in the screen
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
//#include <vector>
#include <math.h>
//#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

int * array;
int * length = new int;

void PrintArray(int * array, int * length);
int *  RequestArray(int * array, int * length);
int *  OrderArray(int * array, int * length);

void FindMax(float *, int length);




int main(){
    
    //cout << *length << endl;
    array = RequestArray(array, length);
    cout << "Before Order" << endl;
    PrintArray(array,length);
    array = OrderArray(array, length);
    cout << "After Order" << endl;
    PrintArray(array,length);
    //FindMax(array,6);
    
    return 0;

}

int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}

void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

