/* 
    Pointers Exercise 3 Show odd number from array and its address
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <vector>
#include <math.h>


using namespace std;
void printVector(vector<int> const &vect);


int main(){

    int num[10];
    vector <int> pares;
    for (int i=0;i<10;i++){
        num[i]= rand() % 100;
    }
    int *dir_num = num;
    
    for (int i = 0; i<(sizeof(num)/sizeof(int)); i++){
        //cout << num[i]<< endl;;
        if ((*dir_num % 2 )==0) {
            cout << "El número " << *dir_num << " es par y se encuentra en la dirección de memoria " << dir_num << endl;
        }
        *dir_num++;
        
    }
    
    return 0;

}

void printVector(vector<int> const &vect){
    cout << "Es divisible por ";

    for (int i=0; i < vect.size(); i++){
        cout << vect.at(i) << ", ";
    }
    cout << "y por si mismo";
    cout << endl;
}