/* 
    Pointers Exercise 16 Pointers to structs. Make an struct called student. With the next fields:
    name, age, avgMark, ask Data for 3 students and check who was the best and print out
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

struct Student {
    string name;
    int age;
    int avgMark;

} p, *Pointer = &p;





int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols);
void FreeMemory(int ** matrix,int Rows, int Cols);
int ** BuildShowMatrix(int ** matrix,int Rows, int Cols);
int ** Traspuesta(int ** matrix,int Rows, int Cols);
void PrintArray(int * array, int * length);
int   AnalyzeArray(char * array, int * length, int * vowelP);
int *  OrderArray(int * array, int * length);
int * FindNumber(int number,int * array, int * length);
void search(int a[],int number,int first, int last);

int main(){
    int numberStudents = 0;
    cout << "Introduce el número de alumnos a registrar " ; cin >> numberStudents;
    Pointer = new struct Student  [numberStudents];

    for (int i = 0; i< numberStudents;i++){
        string aux;
        fflush(stdin);
        cout << "Introduce el nombre de la persona " << endl ; cin >> aux;
        ((Pointer+i))->name = aux;
        
           
        cout << "Introduce la edad de la persona " ; ((Pointer+i))->age = rand() % 25;
        cout << "Introduce la media de la persona " ; ((Pointer+i))->avgMark = rand() % 10;
        cout << "Resumen estudiante" << endl;
        cout << ((Pointer+i))->name << endl;    
        cout << ((Pointer+i))->age << endl;
        cout << ((Pointer+i))->avgMark << endl;
    }
    Student best, *pBest = &best;
    pBest->avgMark = 0 ;
    for (int i = 0; i< numberStudents;i++){
        if((Pointer+i)->avgMark > pBest->avgMark ){
            pBest->avgMark = (Pointer+i)->avgMark ;
            pBest->name =(Pointer+i)->name;
            pBest->age =(Pointer+i)->age;
        }
      
    }
     cout << "Alumno con nota máxima" << endl;
        cout << pBest->name << endl;    
        cout << pBest->age << endl;
        cout << pBest->avgMark << endl;
    return 0;

}


int ** Sum(int ** matrix,int ** matrix2,int Rows, int Cols){
    int **result = new int * [Rows];
    for (int i=0; i<Rows; i++){
        result[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(result+i)+j) = *(*(matrix + i)+j) + *(*(matrix2 + i)+j);
        }
    }

    return result;
}

int ** BuildShowMatrix(int ** matrix,int Rows, int Cols){
    matrix = new int * [Rows];
    for (int i=0; i<Rows; i++){
        matrix[i] = new int [Cols];
        for (int j=0;j<Cols;j++){
            *(*(matrix + i)+j) = rand() % 10;
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            cout << matrix[i][j] << " " ;
        }    
        cout << endl;
        
    }
    return matrix;
}

int ** Traspuesta(int **matrix,int Rows, int Cols){
    int **traspuesta = new int * [Rows];
    
    for (int i=0; i<Rows; i++){
        traspuesta[i] = new int [Cols]; 
    }
    cout << "Pay9o9o" << endl;
    for (int i=0 ;i < Rows; i++){
        for (int j=0;j<Cols;j++){
            
            *(*(traspuesta+j)+i) = *(*(matrix + i)+j);
            //*(matrix[i]+j) = rand() % 100;
            //matrix[i][j] = rand() % 100;
            //cout << matrix[i][j] << " " ;
            cout << traspuesta[i][j] << " ";
        }   
        cout << endl;
    }
    return traspuesta;
}

void FreeMemory(int ** matrix,int Rows, int Cols){
    
    for (int i=0; i<Rows; i++){
        delete[] *(matrix + i);
        //cout << i<< endl;
    }
    delete[] matrix;
}

int  AnalyzeArray(char * array, int * length, int * vowelP){
    int * result = new int;

    for (int i = 0 ; i < *length; i++) {
        //cout << *(array+i) << endl;
        switch (*(array + i)){
            case 'a':
                (*result)++;
                (*vowelP)++;
                break;
            case 'e':
                (*result)++;
                (*(vowelP+1))++;
                break;
            case 'i':
                (*result)++;
                (*(vowelP+2))++;
                break;
            case 'o':
                (*result)++;
                (*(vowelP+3))++;
                break;
            case 'u':
                (*result)++;
                (*(vowelP+ 4))++;
                break;
            default:
                break;
        }
        
    }
    cout << result << " " << *result << endl;
    
    return *result;
}


int *  RequestArray(int * array, int * length){
    
    cout << "Please introduce the length of the array " ; cin >> *length;
    array = new int[*length]; // Crear el arreglo
    for (int i=0;i<*length;i++){
        *(array +i) = rand() % 100;
        cout << array[i] << endl;
    }
    return array;
}

int *  OrderArray(int * array, int * length){
    int min, aux,i,k,j;
    for (i=0;i<*length;i++){
        min = i;
        for (int k=i+1;k<*length;k++){
            if ( *(array+ k) < *(array +min)) {
                min = k;
            }
        }
        aux = array[i];
        array[i] = array[min];
        array[min] = aux;
        
    }
    return array;
}


void intercambio(int &a, int &b){
    int aux;
    aux = a;
    a = b;
    b = aux;
}

void search(int a[],int number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}
void PrintArray(int * array, int * length){
    
    for (int i=0;i<*length;i++){
        
        cout << *(array+i) << endl;
    }
}
void FindMax(float *array, int length){
    float max;
    for (int i = 0; i< length; i++){
        if (i==0){ 
            //max = array[0];
            max = *(array + i);

        } else {
            if (array[i]> max){
                //max = array[i];
                max = *(array+i);
            }
        };
        cout << "Value array " << array[i] << " is in position " << i<< endl;
    }
    cout << "The maximum value for the array is " << max << endl;
}

