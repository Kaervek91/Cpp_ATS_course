/* 
    Crear una cadena que tenga la frase Hola que tal, luego crear otra cadena para preguntarle al usuaruio su nombre y por ultimo añadir el nombre al final de la primera cadena y mostrar el mensaje compelto
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;



int main()
{
    string chain;
    string chain2;
    int number1;
    float number2;
    char *chainP = &chain[0];
    char *chainP2= &chain2[0];
    
    
    cout << "Por favor introduzca un numero entero" << endl;
    getline(cin,chain);
    cout << "Por favor introduzca un numero decimal" << endl;
    getline(cin,chain2);
    number1 = atoi(chainP);
    number2 = atof(chainP2);
    number1 +=number1;
    number2 +=(float)number1;

    cout << "A continuación se presenta el numero entero sumado a si mismo " << number1 << endl;
    cout << "A continuación se presenta el numero decimal sumado con el numero entero " << number2 << endl;
   
    /*cout << chain << endl;
    if (strcmp(chainP,chainP2) == 0){
        cout << "Son palabras iguales" << endl;
    } else {
        cout << "Son palabras distintas" << endl;
    }*/
    
    return 0;

}

