/* 
    Contar el número de vocales 
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;



int main()
{
    string chain;
    
    int counter[5] = {0,0,0,0,0};
    char vowels[5] = {'a','e','i','o','u'};
    char *chainP = &chain[0];
       
    cout << "Por favor introduzca una palabra" << endl;
    getline(cin,chain);
    
    int length = strlen(chainP);
    
    for (int i= 0; i<length-1;i++){
        cout << chain[i]<< endl;
        switch (chain[i]){
            case 'a': counter[0]++; break;
            case 'e': counter[1]++; break;
            case 'i': counter[2]++; break;
            case 'o': counter[3]++; break;
            case 'u': counter[4]++; break;
            default:
                break;
        }
    }
    cout << "Vowels counter:" << endl;
    for (int i= 0;i<5;i++){
        if (counter[i] != 0) {
        cout << "La vocal " << vowels[i] << " tiene " << counter[i] << " elemento/s"<< endl;
        }
    }
    
    return 0;

}

