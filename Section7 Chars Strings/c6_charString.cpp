/* 
    Crear una cadena que tenga la frase Hola que tal, luego crear otra cadena para preguntarle al usuaruio su nombre y por ultimo añadir el nombre al final de la primera cadena y mostrar el mensaje compelto
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;



int main()
{
    string chain;
    string finalchain;
    char *chainP = &chain[0];
    char *chainP2= &finalchain[0];
    
    
    cout << "Por favor introduzca su nombre" << endl;
    getline(cin,chain);
    
    
    int n = strlen(chainP);
    cout << n << endl;
    for (int i = 0; i < n ; i++){
        //cout << i << endl;
        chain[i] = toupper(chain[i]);
        chain[i] = tolower(chain[i]);
        finalchain.append(chain,i,1);
        //cout << finalchain[i] << endl;
    }

    cout << "Final Chain " << finalchain << endl;
   
    cout << chain << endl;
    if (strcmp(chainP,chainP2) == 0){
        cout << "Son palabras iguales" << endl;
    } else {
        cout << "Son palabras distintas" << endl;
    }
    
    return 0;

}

