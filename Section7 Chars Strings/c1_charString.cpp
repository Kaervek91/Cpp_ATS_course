/* 
    Leer una cadena de caracteres del usuario y verificar la longitud de la cadena y si ésta supera los 10 caracteres mostrarla en pantalla de lo contraro no mostrarla
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


using namespace std;


void GetChain(char **chain );

int main()
{
    char chainP[20];
    
    cout << "Introduce una cadena mayor de 10 chars" << endl;
    cin.get(chainP, 20 );
    
    if (strlen(chainP) > 10){
        
        cout << chainP<< endl;
    } else {
        cout << "Chain length is less thant 10, so not displayed" << endl;
    }
    

}



