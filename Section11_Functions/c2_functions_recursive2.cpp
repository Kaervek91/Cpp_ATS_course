/* 
    Functions Exercise 19 - Recursive Sum of n positive integers
    Obtain the result of a factorial number given

    Factorial(n) = 1, si n= 0
    n*factorial(n-1) , si n>0

    FIll data and check the GLOBAL AVERAGE + print all data

    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++14 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>

#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;


int factorial(int n);
int AddPositiveINtegers (int n);

int val;

int resultado = 0;

int main(){
    
    cout << "Introduzca un número positivo " ; 
    
    cin >> val;
   
    resultado = AddPositiveINtegers(val);
    
    cout << "Resultado " << resultado << endl;
    return 0;

}
int AddPositiveINtegers (int n){
    //int aux;
    //cout << "Introduzca un numero positivo para añadir, si quiere salir introduzca cualquier numero negativo "; cin >> aux;
    if (n==1){
        n = 1;
    }
    if (n>1){
        n+=AddPositiveINtegers(n-1);
        cout << n<<  endl;
    }
    return n;
}

int factorial (int n ){
    
    if(n==0){
        n = 1;
    } else{
        n = n * factorial(n-1);
    }
    return n;
}

