/* 
    Functions Exercise 1 and 6 - Plantillas de función permite tipos de dato generales
    Obtain the absolute number from any type of number.
    FIll data and check the GLOBAL AVERAGE + print all data

    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++14 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

template <class TIPOD>
void mostrarAbs(TIPOD numero);
template <class TIPOD>
TIPOD mostrarMax (TIPOD n1,TIPOD n2, TIPOD n3);
template <class TIPOD>
TIPOD val = 0;

int main(){
    int num = 4;
    float num1= 5.5555;
    double num2 = 4.3333333;
    
    
    mostrarAbs(num);
    mostrarAbs(num1);
    mostrarAbs(num2);
    int num3 = 4;
    int num4 = 3;
    int num5 = 6;
    cout << "Max value " << mostrarMax(num3,num4,num5) << endl;
    return 0;

}

template <class TIPOD>
void mostrarAbs (TIPOD numero){
    if(numero<0){
        numero = numero * -1;
    }
    cout << "El valor absoluto del numero es " << numero << endl;
    
}

template <class TIPOD>
TIPOD mostrarMax (TIPOD n1,TIPOD n2, TIPOD n3){
    TIPOD max;
    if (n1 > n2 && n1>n3){
        max = n1;
    } else if (n2 > n3){
        max = n2;
    }
    else {
        max = n3;
    }
}