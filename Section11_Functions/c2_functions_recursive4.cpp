/* 
    Functions Exercise 21 - Recursive Calculate number elevate to a state mayor or equal to cero
    Obtain the result of a factorial number given

    0,1,1,2,3,5,8,13,21,34..


    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++14 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>

#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

int elevate(int x,int n);
int factorial(int n);
int AddPositiveINtegers (int n);

int val;

int resultado = 0;

int main(){
    
    cout << "Introduzca un número  " ; 
    int pot = 0;
    
    cin >> val;
    
    cout << "Introduzca su potencia ";
    cin >>pot;

    cout << val;
    cout << pot;
    resultado = elevate(val,pot);
    
    cout << "Resultado " << resultado << endl;
    return 0;

}

int elevate(int x, int n){
    if (n==0){
        x = 1;
    }
    if (n>= 1){
        x *= elevate(x,n-1);

    }
    return x;
}
int AddPositiveINtegers (int n){
    //int aux;
    //cout << "Introduzca un numero positivo para añadir, si quiere salir introduzca cualquier numero negativo "; cin >> aux;
    if (n==1){
        n = 1;
    }
    if (n>1){
        n+=AddPositiveINtegers(n-1);
        cout << n<<  endl;
    }
    return n;
}

int factorial (int n ){
    
    if(n==0){
        n = 1;
    } else{
        n = n * factorial(n-1);
    }
    return n;
}

