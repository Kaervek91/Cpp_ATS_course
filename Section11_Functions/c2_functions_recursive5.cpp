/* 
    Functions Exercise 22 - Recursive Write numbes from init to end
    Obtain the result of a factorial number given

    0,1,1,2,3,5,8,13,21,34..


    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++14 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>

#include <stdlib.h> // Necesaria para el new y el delete ??


using namespace std;

int Printnumbers(int init, int end);
int elevate(int x,int n);
int factorial(int n);
int AddPositiveINtegers (int n);

int init;

int resultado = 0;

int main(){
    
    cout << "Introduzca un inicio  " ; 
    int end = 0;
    
    cin >> init;
    
    cout << "Introduzca su final ";
    cin >>end;

    
    cout << Printnumbers(init ,end) << " ";
    
    
    return 0;

}

int Printnumbers(int init, int end){
    int n;
    
    if (init == end){
        n = end;
    } else {
        cout << init  << " ";
        n = Printnumbers(init + 1, end);
    }
    
    return n;
}

int elevate(int x, int n){
    if (n==0){
        x = 1;
    }
    if (n>= 1){
        x *= elevate(x,n-1);

    }
    return x;
}
int AddPositiveINtegers (int n){
    //int aux;
    //cout << "Introduzca un numero positivo para añadir, si quiere salir introduzca cualquier numero negativo "; cin >> aux;
    if (n==1){
        n = 1;
    }
    if (n>1){
        n+=AddPositiveINtegers(n-1);
        cout << n<<  endl;
    }
    return n;
}

int factorial (int n ){
    
    if(n==0){
        n = 1;
    } else{
        n = n * factorial(n-1);
    }
    return n;
}

