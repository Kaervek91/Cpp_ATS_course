//Calculate hypotenus from 2 cateto of triangle

#include <iostream>
#include <math.h>

using namespace std;

int main () {

    float hypotenus, cateto1,cateto2 = 0;
    cout << "Calculate hypotenus by giving 2 catetos\n";
    cout << "Enter cateto 1: ";cin >> cateto1;
    cout << "Enter cateto 2: ";cin >> cateto2;
    hypotenus  = sqrt(pow(cateto1,2)  + pow(cateto2,2));
    cout.precision(2);
    cout << "Hypotenus of the triangle : " << hypotenus << endl;
    return 0;
}