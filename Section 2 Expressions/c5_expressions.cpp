//Write an algoritm that interchange two values between them.

#include <iostream>

using namespace std;

int main () {

    float a,b,aux;
    cout << "Interchange two values\n";
    cout << "Enter parameter A: ";cin >> a;
    cout << "Enter parameter B: ";cin >> b;
    aux = a;
    a = b;
    b = aux;
    cout << "Result: A = " << a << " y B = " << b << endl;
    return 0;
}