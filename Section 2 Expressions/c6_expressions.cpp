//Show and calculate the average of marks from 4 students

#include <iostream>

using namespace std;

int main () {

    float a,b,c,d,average = 0;
    cout << "Calculates the average of 4 marks\n";
    cout << "Enter mark of student A: ";cin >> a;
    cout << "Enter mark of student B: ";cin >> b;
    cout << "Enter mark of student C: ";cin >> c;
    cout << "Enter mark of student D: ";cin >> d;

    average = (a + b + c + d)/4;
    cout.precision(2);
    cout << "Mark of student A : " << a << endl;
    cout << "Mark of student B : " << b << endl;
    cout << "Mark of student C : " << c << endl;
    cout << "Mark of student D : " << d << endl;
    cout << "Average of the 4 students : " << average << endl;
    return 0;
}