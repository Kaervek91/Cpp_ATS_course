//Write expression a/b + 1

#include <iostream>

using namespace std;

int main () {

    float a,b,result = 0;
    cout << "Calculates the next expression A/B+1\n";
    cout << "Enter parameter A: ";cin >> a;
    cout << "Enter parameter B: ";cin >> b;
    result = a/b +1;
    cout.precision(2);
    cout << "Result: " << result << endl;
    return 0;
}