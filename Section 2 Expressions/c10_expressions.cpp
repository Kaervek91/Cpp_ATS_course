//Calculate Solutions for a second grade equation ax²+bx+c = 0

#include <iostream>
#include <math.h>

using namespace std;

int main () {

    float c,a,b,x1,x2 = 0;
    cout << "Calculate Solutions for a second grade equation ax²+bx+c = 0\n";
    cout << "Enter a: ";cin >> a;
    cout << "Enter b: ";cin >> b;
    cout << "Enter c: ";cin >> c;
    x1  = (-b+sqrt(pow(b,2)-4*a*c))/(2*a);
    x2  = (-b-sqrt(pow(b,2)-4*a*c))/(2*a);
    cout.precision(2);
    cout << "RESULT 1: " << x1 << endl;
    cout << "RESULT 2: " << x2 << endl;
    return 0;
}