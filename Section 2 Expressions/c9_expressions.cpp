//Calculate f(x,y)= sqrt(x)/(y²-1)

#include <iostream>
#include <math.h>

using namespace std;

int main () {

    float result, y,x = 0;
    cout << "Calculate the function f(x,y)= sqrt(x)/(y²-1)\n";
    cout << "Enter x: ";cin >> x;
    cout << "Enter y: ";cin >> y;
    result  = sqrt(x)/(pow(y,2)-1);
    cout.precision(2);
    cout << "RESULT : " << result << endl;
    return 0;
}