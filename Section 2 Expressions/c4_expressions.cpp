//Write expression a + b/(c-d)

#include <iostream>

using namespace std;

int main () {

    float a,b,c,d,result = 0;
    cout << "Calculates the next expression A/B+1\n";
    cout << "Enter parameter A: ";cin >> a;
    cout << "Enter parameter B: ";cin >> b;
    cout << "Enter parameter C: ";cin >> c;
    cout << "Enter parameter D: ";cin >> d;
    result = a+ b/(c-d);
    cout.precision(2);
    cout << "Result: " << result << endl;
    return 0;
}