//Show average of final mark from a student

#include <iostream>

using namespace std;

int main () {

    float practices,theory,classes, finalMark = 0;
    cout << "Calculate final mark of a student\n";
    cout << "Enter practices mark : ";cin >> practices;
    cout << "Enter mark of student B: ";cin >> theory;
    cout << "Enter mark of student C: ";cin >> classes;
    

    finalMark = practices*0.3 + theory*0.6 + classes*0.1;
    cout.precision(2);
    cout << "Final Mark of the student : " << finalMark << endl;
    return 0;
}