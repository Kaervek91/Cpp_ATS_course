/* Pointer Class Definition Header */
#include "iostream"

const int MAX_X = 20;
const int MAX_Y = 5;
enum movements{
    NONE = 0,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    OUT
};

using namespace std;

class Pointer {
    private:
        int posX,posY;
        int lastMove = NONE;

    public:
        Pointer(){
            this->posX = 0;
            this->posY = 0;
        }
        
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Pointer(){};
        
        void moveUp(){
            int tempY = this->getPosY();
            tempY--;
            if (tempY > -MAX_Y && tempY < MAX_Y){
                this->setPosY(tempY);
            } else {
                cout << "\n\t WARNING: Invalid movement\n";
            }
        }

        void moveDown(){
            int tempY = this->getPosY();
            tempY++;
            if (tempY > -MAX_Y && tempY < MAX_Y){
                this->setPosY(tempY);
            } else {
                cout << "\n\t WARNING: Invalid movement\n";
            }
        }

        void moveLeft(){
            int tempX = this->getPosX();
            tempX--;
            if (tempX <= -MAX_X){
                this->setPosX(-MAX_X+1);
            } else {
                this->setPosX(tempX);
                //cout << "\n\t WARNING: Invalid movement\n";
            }
        }
        
        void moveRight(){
            int tempX = this->getPosX();
            tempX++;
            if (tempX >= MAX_X){
                this->setPosX(MAX_X-1);
            } else {
                this->setPosX(tempX);
                //cout << "\n\t WARNING: Invalid movement\n";
            }
        }

        void moveOut(){
            this->setMove(OUT);
        }

        void setPosX(int posX){
            this->posX = posX;    
        }

        void setPosY(int posY){
            if (posY <= MAX_Y){
                this->posY = posY;
            } else {
                cout << "\n\t WARNING: Invalid movement\n";
            }
        }
        
        void setMove(int move){
            this->lastMove = move;
        }

        int getMove (){
            return this->lastMove;
        }
        int getPosX(){
            return this->posX;
        };

        int getPosY(){
            return this->posY;
        };

        void menu(){
            cout << "hello\n";
        }
};