/* 
    Funciones clase Table
    
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include "Clase_Table.h"
#include "stdio.h"


using namespace std;

Table::Table(){};

// Build Board

void Table::build_table()
{
    int PosX = this->pointer.getPosX() + 20;
    int PosY = this->pointer.getPosY() + 5;

    for (int y = 0;y<= MAX_Y_TABLE;y++){
        for(int x = 0; x <= MAX_X_TABLE;x++){
            if(PosX == x && PosY == y){
                cout << "x";
            } else {
                if(x==0 || x == MAX_X_TABLE){
                    cout<< "*";
                } else if (y== 0 || y == MAX_Y_TABLE){
                    cout << "*";
                } else{
                    cout << " ";
                }
            }
        }
        cout<< "\n";
    }
}

void Table::detectKeys(){
    char c = getchar();
    switch (c){
        case 'a':
            this->pointer.moveLeft();
            break;
        case 's':
            this->pointer.moveDown();
            break;
        case 'w':
            this->pointer.moveUp();
            break;
        case 'd':
            this->pointer.moveRight();
            break;
        case 'q':
            this->pointer.moveOut();
            break;
                
    }
}

void Table::startGame(){
    do {
        this->detectKeys();
        this->build_table();
    } while (this->pointer.getMove() != OUT);
}

 

Table::~Table() {}

