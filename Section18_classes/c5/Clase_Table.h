/* List Class Definition Header */

#ifndef TABLE_H
#define TABLE_H
#include "iostream"
#include "stdlib.h"
#include "Clase_Pointer.h"

const int MAX_X_TABLE = 40;
const int MAX_Y_TABLE = 10;

using namespace std;



class Table {
    
    public:
        // Método constructor
        Table();
        
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Table();
        
        //Atributos públicos
        Pointer pointer;

        // Métodos públicos
        void startGame();
        void build_table();
        void detectKeys();
        
};

                

#endif