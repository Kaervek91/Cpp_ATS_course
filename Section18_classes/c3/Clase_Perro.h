/* Perro Class Definition Header */

#include <string>

class Perro {
    private:
        std::string nombre,raza;

    public:
        Perro(){ // inicializa los objetos, para crear arreglos de objetos necesitamos el constructor por defecto
        }
        // Pero siempre puedes poner otro constructor
        Perro(std::string _nombre, std::string _raza){
            nombre = _nombre;
            raza = _raza;
        }
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Perro(){};
        
        void asignarNombre();
        void asignarRaza();
        void mostrarDatos();
        void jugar();
};