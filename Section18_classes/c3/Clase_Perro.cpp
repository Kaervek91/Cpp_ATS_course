/*
*   Perro FUNCTIONS
*/
#include "Clase_Perro.h"
#include <iostream>
#include <string>


void Perro::asignarNombre(){
    std::cout << "Introduce el nombrel del perro ";
    std::getline(std::cin,this->nombre); //Hace referencia a los miembros de la clase si tuvieramos otras variables del mismo nombre permite diferenciar
}

void Perro::asignarRaza(){
    std::cout << "Introduce la raza del perro ";
    if(nombre.length() > 1) { std::cout << "llamado " << nombre << " ";};
    std::getline(std::cin,this->raza);
}

void Perro::mostrarDatos(){
    std::cout << "El nombre del perro es " << nombre << std::endl;
    std::cout << "La raza del perro es " << raza << std::endl;
}

void Perro::jugar(){
    std::cout << "El perro " << nombre << "está jugando" << std::endl;
}