/* 
    Example 3 - Destructors of objects
    Example 4 - This autoreference
    
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <limits>
#include <string>
#include "Clase_Perro.h"


using namespace std;



int main(){
    Perro *perrera;
    int number;
    cout << "Introduce el número de perros a registrar " << endl ;
    cin >> number;
    cin.ignore( numeric_limits <streamsize> ::max(), '\n' );
    perrera = new Perro[number];
    //cout << sizeof(alumnos) << endl;
    //cout << sizeof(Alumno) << endl;
    /* Registrar los perros*/
    cout << "Registre los perros" << endl; 
    for (int i=0 ; i<number;i++){
        cout << "Perro " << i << endl;
        perrera[i].asignarNombre();
        perrera[i].asignarRaza();
        cout << endl;
    };
    /* Mostar los perros*/
    cout << "Mostrar los perros" << endl; 
    for (int i=0 ; i<number;i++){
        cout << "Perro " << i << endl;
        perrera[i].mostrarDatos();
        cout << endl;
    };

    // Eliminamos un objeto.
    perrera[2].~Perro();
    number--;

    /* Mostar los perros*/
    cout << "Mostrar los perros" << endl; 
    for (int i=0 ; i<number;i++){
        cout << "Perro " << i << endl;
        perrera[i].mostrarDatos();
        cout << endl;
    };
    // Objeto de tipo dinámico
    Perro* perroDynamic = new Perro("Pedrito", "Chichuaha");
    perroDynamic->mostrarDatos();
    delete perroDynamic;

    return 0;

}