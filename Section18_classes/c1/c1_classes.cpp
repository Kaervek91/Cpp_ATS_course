/* 
    Exercise 1 - Define a class Birthday 
        Attributes: Day, month
        Methods: equal() view()
        Write a program that check if a date is your birthday
    
    &n = address of n
    *n = value stored in its address n
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Birthday.h"


using namespace std;


int main(){
    int day,month;
    Birthday guessBirthdayGame;
    
    
    cout << "Initial value Birthday date " << endl ;
    guessBirthdayGame.printBirthday();

    cout << "Insert a new date, first the day (1-31)" ; 
    cin >> day;
    cout << "Now the month (1-12)" ;
    cin >> month;
    guessBirthdayGame.setDate(day,month);

    guessBirthdayGame.printBirthday();

    cout << "Guess the Birthday, first the day (1-31)" ; 
    cin >> day;
    cout << "Now the month (1-12)" ;
    cin >> month;

    if (guessBirthdayGame.isBirthday(day,month)){
        cout << "YOU WIN!" << endl;
    }else {
        cout << "WRONG DATE"<< endl;
    }



    return 0;

}