/* Birthday Class Definition Header */



class Birthday {
    private:
        int day;
        int month;

        bool isvalidDate(int _day,int _month);
        bool equal(int _day,int _month);

    public:
        Birthday(){
            day = 1;
            month = 1;
        }
        Birthday(int _day, int _month){
            day = _day;
            month = _month;
        }
        
        bool isBirthday(int _day,int _month);

        int printBirthday();

        void setDate(int _day,int _month);
};