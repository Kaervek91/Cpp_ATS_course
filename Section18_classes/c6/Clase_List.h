/* List Class Definition Header */
#ifndef LIST_H
#define LIST_H


#include "Clase_Node.h"
#include "iostream"
#include "stdlib.h"


using namespace std;

template <class T>

class List {
    private:
        Node<T> *m_head;
        int m_num_nodes;
        short action;
        T currentObject;

    public:
        // Método constructor
        List();
        
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~List(){};
        
        // Métodos públicos
    
        void add_head(T);
        void add_end(T);
        void add_sort(T);
        void concat(List);
        void del_all();
        void del_by_data(T);
        void del_by_position(int);
        void fill_by_user(int);
        void fill_random(int);
        void intersection(List);
        void invert();
        void load_file(string);
        void printAll();
        void print();
        void save_file(string);
        List<T> search();
        void printList(List<T>);
        void sort();
        void menu();
        void start();
        void exitProgram();
        void setAction();
        short getAction();
        int getNumNodes();
        
};

#endif