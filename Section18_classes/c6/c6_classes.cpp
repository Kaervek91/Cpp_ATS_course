/* 
    Exercise 4 - Build a program that store cars and show them by type, brand, price etc
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include "Clase_Auto.h"
#include "Clase_List.h"
#include "Clase_List.cpp"
#include "Clase_Node.cpp"


using namespace std;


int main(){
    
    List<Auto> ListAuto;
    ListAuto.start();
    
    return 0;

}

