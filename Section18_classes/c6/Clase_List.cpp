/* 
    Funciones Clase List
    
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include "Clase_List.h"


using namespace std;

// Constructor por defecto
template<typename T>
List<T>::List()
{
    m_num_nodes = 0;
    m_head = NULL;
}
 
// Insertar al inicio
template<typename T>
void List<T>::add_head(T data_)
{
    Node<T> *new_node = new Node<T> (data_);
    Node<T> *temp = m_head;
    if (!m_head) {
        m_head = new_node;
    } else {
        new_node->next = m_head;
        m_head = new_node;
 
        while (temp) {
            temp = temp->next;
        }
    }
    m_num_nodes++;
}
 
// Insertar al final
template<typename T>
void List<T>::add_end(T data_)
{
    Node<T> *new_node = new Node<T> (data_);
    Node<T> *temp = m_head;
 
    if (!m_head) {
        m_head = new_node;
    } else {
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = new_node;
    }
    m_num_nodes++;
}
 
// Insertar de manera ordenada
template<typename T>
void List<T>::add_sort(T data_)
{
    Node<T> *new_node = new Node<T> (data_);
    Node<T> *temp = m_head;
 
    if (!m_head) {
        m_head = new_node;
    } else {
        if (m_head->data > data_) {
            new_node->next = m_head;
            m_head = new_node;
        } else {
            while ((temp->next != NULL) && (temp->next->data < data_)) {
                temp = temp->next;
            }
            new_node->next = temp->next;
            temp->next = new_node;
        }
    }
    m_num_nodes++;
}
 
// Concatenar a otra List
template<typename T>
void List<T>::concat(List list)
{
    Node<T> *temp2 = list.m_head;
 
    while (temp2) {
        add_end(temp2->data);
        temp2 = temp2->next;
    }
}
 
// Eliminar todos los nodos
template<typename T>
void List<T>::del_all()
{
    m_head->delete_all();
    m_num_nodes = 0;
    m_head = NULL;
}
 
// Eliminar por data del nodo
template<typename T>
void List<T>::del_by_data(T data_)
{
    Node<T> *temp = m_head;
    Node<T> *temp1 = m_head->next;
 
    int cont = 0;
 
    if (m_head->data == data_) {
        m_head = temp->next;
    } else {
        while (temp1) {
            if (temp1->data == data_) {
                Node<T> *aux_node = temp1;
                temp->next = temp1->next;
                delete aux_node;
                cont++;
                m_num_nodes--;
            }
            temp = temp->next;
            temp1 = temp1->next;
        }
    }
 
    if (cont == 0) {
        cout << "No existe el dato " << endl;
    }
}
 
// Eliminar por posición del nodo
template<typename T>
void List<T>::del_by_position(int pos)
{
    Node<T> *temp = m_head;
    Node<T> *temp1 = temp->next;
 
    if (pos < 1 || pos > m_num_nodes) {
        cout << "Fuera de rango " << endl;
    } else if (pos == 1) {
        m_head = temp->next;
    } else {
        for (int i = 2; i <= pos; i++) {
            if (i == pos) {
                Node<T> *aux_node = temp1;
                temp->next = temp1->next;
                delete aux_node;
                m_num_nodes--;
            }
            temp = temp->next;
            temp1 = temp1->next;
        }
    }
}
 
// Llenar la Lista por teclado
/*template<typename T>
void List<T>::fill_by_user(int dim)
{
    T ele;
    for (int i = 0; i < dim; i++) {
        cout << "Ingresa el el//prueba.filter();
    //OutputList->add();emento " << i + 1 << endl;
        cin >> ele;
        add_end(ele);
    }
}
 
// Llenar la Lista aleatoriamente para enteros
template<typename T>
void List<T>::fill_random(int dim)
{
    srand(time(NULL));
    for (int i = 0; i < dim; i++) {
        add_end(rand() % 100);
    }
}
 
// Usado por el método intersección
template<typename T>
void insert_sort(T a[], int size)
{
    T temp;
    for (int i = 0; i < size; i++) {
        for (int j = i-1; j>= 0 && a[j+1] < a[j]; j--) {
            temp = a[j+1];
            a[j+1] = a[j];
            a[j] = temp;
        }
    }
}
 
// Números que coinciden en 2 Lists
template<typename T>
void List<T>::intersection(List list_2)
{
    Node<T> *temp = m_head;
    Node<T> *temp2 = list_2.m_head;
 
    // Creo otra Lista
    List intersection_list;
 
    int num_nodes_2 = list_2.m_num_nodes;
    int num_inter = 0;
 
    // Creo 2 vectores dinámicos
    T *v1 = new T[m_num_nodes];
    T *v2 = new T[num_nodes_2];
 
    // Lleno los vectores v1 y v2 con los datas de la lista original y segunda lista respectivamente
    int i = 0;
 
    while (temp) {
        v1[i] = temp->data;
        temp = temp->next;
        i++;
    }
 
    int j = 0;
 
    while (temp2) {
        v2[j] = temp2->data;
        temp2 = temp2->next;
        j++;
    }
 
    // Ordeno los vectores
    insert_sort(v1, m_num_nodes);
    insert_sort(v2, num_nodes_2);
 
    // Índice del 1er vector (v1)
    int v1_i = 0;
 
    // Índice del 2do vector (v2)
    int v2_i = 0;
 
  // Mientras no haya terminado de recorrer ambas Lists
  while (v1_i < m_num_nodes && v2_i < num_nodes_2) {
      if (v1[v1_i] == v2[v2_i]) {
          intersection_list.add_end(v1[v1_i]);
          v1_i++;
          v2_i++;
          num_inter++;
      } else if (v1[v1_i] < v2[v2_i]) {
          v1_i++;
      } else {
          v2_i++;
      }
  }
 
  // Solo si hay alguna intersección imprimo la nueva lista creada
  if (num_inter > 0) {
      cout << "Existen " << num_inter << " intersecciones " << endl;
      intersection_list.print();
  } else {
      cout << "No hay intersección en ambas listas" << endl;
  }
}
 
// Invertir la lista
template<typename T>
void List<T>::invert()
{
    Node<T> *prev = NULL;
    Node<T> *next = NULL;
    Node<T> *temp = m_head;
 
    while (temp) {
        next = temp->next;
        temp->next = prev;
        prev = temp;
        temp = next;
    }
    m_head = prev;
}
 */
/*/ Cargar una lista desde un archivo
template<typename T>
void List<T>::load_file(string file)
{
    T line;
    ifstream in;
    in.open(file.c_str());
 
    if (!in.is_open()) {
        cout << "No se puede abrir el archivo: " << file << endl << endl;
    } else {
        while (in >> line) {
            add_end(line);
        }
    }
    in.close();
}*/
 
// Print whole List
template<typename T>
void List<T>::printAll()
{
    cout<< "\n\t--- Start printing the List ---\n";
    Node<T> *temp = m_head;
    if (!m_head) {
        cout << "Empty List" << endl;
    } else {
        while (temp) {
            temp->print();
            if (!temp->next) cout << "\n\t--- End of the List ---\n";
            temp = temp->next;
        }
    }
    cout << endl << endl;
}
 
// Buscar el dato de un nodo
template<typename T>
List<T> List<T>::search()
{
    Node<T> *temp = this->m_head;
    List<T> OutputList;
    this->currentObject = this->m_head->data.filter();
    this->currentObject.setPrice(0);
    
    // 1. Create filter specific on Auto CLass
    // 2. Search in official list
    // 3. Findings will be added to new list and returned

    while (temp) {
        if (this->currentObject.getBrand_filter() != "" || this->currentObject.getModel_filter() != "" || this->currentObject.getType_filter() != "None") {
            if (temp->data.getBrand() == this->currentObject.getBrand_filter() ||
            temp->data.getModel() == this->currentObject.getModel_filter() ||
            temp->data.getType() == this->currentObject.getType_filter()) {
                cout << "\nADDING 1\n";
                OutputList.add_head(temp->data);
            }
        } else if (this->currentObject.getCheapest_filter()){
            if(this->currentObject.getPrice() == 0) {
                this->currentObject.setBrand(temp->data.getBrand());
                this->currentObject.setModel(temp->data.getModel());
                this->currentObject.setType(temp->data.getType());
                this->currentObject.setPrice(temp->data.getPrice());
            } else {
                if (this->currentObject.getPrice() > temp->data.getPrice()){
                    this->currentObject.setBrand(temp->data.getBrand());
                    this->currentObject.setModel(temp->data.getModel());
                    this->currentObject.setType(temp->data.getType());
                    this->currentObject.setPrice(temp->data.getPrice());
                }
            }
            if (!temp->next) {
                cout << "\nADDING 2\n";
                OutputList.add_head(this->currentObject);
            }

        } else if (this->currentObject.getExpensive_filter()){
            if(this->currentObject.getPrice() == 0) {
                this->currentObject.setBrand(temp->data.getBrand());
                this->currentObject.setModel(temp->data.getModel());
                this->currentObject.setType(temp->data.getType());
                this->currentObject.setPrice(temp->data.getPrice());
            } else {
                if (this->currentObject.getPrice() < temp->data.getPrice()){
                    this->currentObject.setBrand(temp->data.getBrand());
                    this->currentObject.setModel(temp->data.getModel());
                    this->currentObject.setType(temp->data.getType());
                    this->currentObject.setPrice(temp->data.getPrice());
                }
            }
            if (temp->next == NULL) {
                cout << "\nADDING3\n";
                OutputList.add_head(this->currentObject);
            }
        } else if (this->currentObject.getPriceMax_filter() != 0 || this->currentObject.getPriceMin_filter() != 0) {

        }
        
        temp = temp->next;
        
    }
    //reset filters
    
    return OutputList;
}
 
// Ordenar de manera ascendente
template<typename T>
void List<T>::sort()
{
    T temp_data;
    Node<T> *aux_node = m_head;
    Node<T> *temp = aux_node;
 
    while (aux_node) {
        temp = aux_node;
 
        while (temp->next) {
            temp = temp->next;
 
            if (aux_node->data > temp->data) {
                temp_data = aux_node->data;
                aux_node->data = temp->data;
                temp->data = temp_data;
            }
        }
 
        aux_node = aux_node->next;
    }
}

template<typename T>
int List<T>::getNumNodes(){
    return m_num_nodes;
}

template<typename T>
void List<T>::menu(){

    while(true) {
        
        cout << "\n\t -- Choose one option please: -- \n" << \
            "\t 1. Create object\n";
        if(getNumNodes() != 0){
            /* Basic menu */
            cout << "\t 2. Delete all\n" <<  \
                "\t 3. Print all\n" \
                "\t 4. Search data\n" ;
            //m_head->data->menu();
        }
        cout << "\t 0. Exit\n"  ;
        this->setAction();
        /* Specific functions */
        switch(this->getAction()){
            case 1:
                this->currentObject.createObject();
                this->add_head(this->currentObject);
                break;
            case 2:
                if (this->getNumNodes() != 0){
                    cout<< "Deleting all objects " << endl;
                    del_all();
                    cout << "Deleted" << endl;
                } else {
                    cout << "Nothing to erase" << endl;
                }
                break;
            case 3:
                this->printAll();
                break;
            case 4: /* Search data by type filtering */
                this->printList(this->search());
                break;                
            /*case 5:
                cout << "Looking for the richest client!" << endl;
                printUser(richest(list));
                break;
            case 6:
                cout << "Total money in bank is " << endl;
                cout << totalMoneyBank(list);
                break;
            case 7:
                cout << "Total number of clients!" << endl;
                cout << numberOfClients(list);
                break;*/
            case 0:
                this->exitProgram();
                break;
            default:
                cout << "You entered a wrong selection" << endl;
                break;
        }
    }


}


template<typename T>
void List<T>::printList(List<T> auxList){
    cout<< "\n\t--- Start printing the List ---\n";
    Node<T> *temp = auxList.m_head;
    if (!m_head) {
        cout << "Empty List" << endl;
    } else {
        while (temp) {
            temp->print();
            if (!temp->next) cout << "\n\t--- End of the List ---\n";
            temp = temp->next;
        }
    }
    cout << endl << endl;
}

template<typename T>
void List<T>::start(){
    menu();
}

template<typename T>
void List<T>::exitProgram(){
    cout << "Closing program!" << endl;
    exit(EXIT_SUCCESS);
}

template<typename T>
void List<T>::setAction(){
    cin >> this->action;
}

template<typename T>
short List<T>::getAction(){
    return this->action;
}

/*/ Guardar una lista en un archivo
template<typename T>
void List<T>::save_file(string file)
{
    Node<T> *temp = m_head;
    ofstream out;
    out.open(file.c_str());
 
    if (!out.is_open()) {
        cout << "No se puede guardar el archivo " << endl;
    } else {
        while (temp) {
            out << temp->data;
            out << " ";
            temp = temp->next;
        }
    }
    out.close();
}*/