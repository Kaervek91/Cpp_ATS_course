/* Auto Class Definition Header */
#include "iostream"

using namespace std;

class Auto {
    private:
        struct {
            string brand, model, type;
            float price;
        } data;
        struct {
            string brand = "", model = "", type = "None";
            float priceMax= 0;
            float priceMin = 0;
            bool cheapest=false,expensive=false;
        } filter_data;
        
    public:
        Auto(){
            this->data.brand = "";
            this->data.model = "";
            this->data.type = "";
            this->data.price = 0;
        }
        Auto(string brand, string model, string type, float price){ // inicializa los objetos, para crear arreglos de objetos necesitamos el constructor por defecto
            this->data.brand = brand;
            this->data.model = model;
            this->data.type = type;
            this->data.price = price;
        }
        
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Auto(){};
        
        void createObject(){
            string brand,model;
            int type = 0;
            float price;
            
            cout << "Insert auto brand: ";
            //fflush(stdin);
            cin.ignore();
            getline(cin, brand);
            this->setBrand(brand);
            cout << "Insert auto model: ";
            //cin.ignore();
            getline(cin, model);
            this->setModel(model);
            cout << "Choose auto type:\n" \
                "\t1- SUV\n\t2- Compact\n\t3- Sport\n\t4- 4x4\n\t5- Berline" << endl;
            do{
                cin >> type;
                switch(type) {
                    case 1:
                        this->setType("SUV");
                        break;
                    case 2:
                        this->setType("Compact");
                        break;
                    case 3:
                        this->setType("Sport");
                        break;
                    case 4:
                        this->setType("4x4");
                        break;
                    case 5:
                        this->setType("Berline");
                        break;
                    default:
                        cout << "Incorrect value try between the next ones \n\t1- SUV\n\t2- Compact\n\t3- Sport\nt4- 4x4\n";
                        break;
                }
            }while(type<0 && type>6);

            cout << "Insert price: ";
            cin >> price;
            this->setPrice(price);
        }

        void createObject_filter(Auto* object){
            string brand,model;
            int type = 0;
            int option;
            float price;
            this->filterMenu();
            cin >> option;
            do {
                switch (option) {
                    case 1:
                        // Search by brand model or type
                        cout << "Stablish brand, model or type selected if none of them is left as blanck won't be used for filtering\n";
                        cout << "Insert auto brand: ";
                        //fflush(stdin);
                        cin.ignore();
                        getline(cin, brand);
                        if (model != "") object->setBrand_filter(brand);
                        cout << "Insert auto model: ";
                        //cin.ignore();
                        getline(cin, model);
                        if (model != "")  object->setModel_filter(model);
                        cout << "Choose auto type:\n" \
                            "\t1- SUV\n\t2- Compact\n\t3- Sport\n\t4- 4x4\n\t5- Berline\n\t6- None" << endl;
                        do{
                            cin >> type;
                            switch(type) {
                                case 1:
                                    this->setType_filter("SUV"); break;
                                case 2:
                                    this->setType_filter("Compact"); break;
                                case 3:
                                    this->setType_filter("Sport"); break;
                                case 4:
                                    this->setType_filter("4x4"); break;
                                case 5:
                                    this->setType_filter("Berline"); break;
                                case 6:
                                    this->setType_filter("None");break;
                                default:
                                    cout << "Incorrect value try between the next ones \n\t1- SUV\n\t2- Compact\n\t3- Sport\nt4- 4x4\n\t5- Berline\n\t6- None"; break;
                            }
                        }while(type<0 && type>7);
                        break;
                    case 2:
                        // Search in a range of prices
                        float max,min;
                        cout << "You can stablish the range of prices you want to search your car, if you stablish 0 will be understood as none limit\n";
                        cout << "\n\tMax car value: ";
                        cin >> max;
                        object->setPriceMax_filter(max);
                        cout << "\n\tMin car value: ";
                        cin >> min;
                        object->setPriceMin_filter(min);
                        break;
                    case 3:
                        // Search cheapest
                        object->setCheapest_filter(true);
                        break;
                    case 4:
                        // Search the most expensive
                        object->setExpensive_filter(true);
                        break;
                    case 5:
                        // Cancel operation with no results
                        cout << "Operation cancelled none filter set\n";
                        break;
                    default:
                        cout << "\n\tWrong option choose again between the valid options\n" << endl;
                        object->filterMenu();
                        break;
                    }
            }while (option<0 && option>5);

        }

        void setBrand(string brand){
            this->data.brand = brand;
        }

        void setModel(string model){
            this->data.model = model;
        }

        void setType (string type){
            this->data.type = type;
        }

        void setPrice (float price){
            this->data.price = price;
        }

        float getPrice(){
            return this->data.price;
        };

        string getBrand(){
            return this->data.brand;
        };

        string getModel(){
            return this->data.model;
        };

        string getType(){
            return this->data.type;
        };

        void setBrand_filter(string brand){
            this->filter_data.brand = brand;
        }

        void setModel_filter(string model){
            this->filter_data.model = model;
        }

        void setType_filter (string type){
            this->filter_data.type = type;
        }

        void setCheapest_filter (bool cheapest){
            this->filter_data.cheapest = cheapest;
        }

        void setExpensive_filter (bool expensive){
            this->filter_data.expensive = expensive;
        }

        void setPriceMax_filter (float price){
            this->filter_data.priceMax = price;
        }

        void setPriceMin_filter (float price){
            this->filter_data.priceMin = price;
        }

        float getPriceMax_filter(){
            return this->filter_data.priceMin;
        };

        float getPriceMin_filter(){
            return this->filter_data.priceMin;
        };

        string getBrand_filter(){
            return this->filter_data.brand;
        };

        string getModel_filter(){
            return this->filter_data.model;
        };

        string getType_filter(){
            return this->filter_data.type;
        };

        bool getCheapest_filter(){
            return this->filter_data.cheapest;
        };

        bool getExpensive_filter(){
            return this->filter_data.expensive;
        };

        void print(){
            cout << "\n\t ---  ";
            cout << "Auto";
            cout << " data --- \n";
            cout << " - Brand " << this->data.brand << endl;
            cout << " - Model " << this->data.model << endl;
            cout << " - Type " << this->data.type << endl;
            cout << " - Price " << this->data.price << endl;
        };
        
        Auto filter(){
            Auto autoFilter;
            this->createObject_filter(&autoFilter);
            
            return autoFilter;
        }

        void filterMenu(){
            cout << "\n\tFiltering Options\n" \
                "\t1. By brand|model|type\n" \
                "\t2. By price\n" \
                "\t3. Cheapest\n" \
                "\t4. Most expensive\n" \
                "\t5. Cancel\n" ;
        }
};