/* 
    Example 2 - Array of objects
        
    
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Clase_Alumno.h"


using namespace std;



int main(){
    Alumno *alumnos;
    int number;
    cout << "Introduce el número de alumnos a registrar " << endl ;
    cin >> number;
    alumnos = new Alumno[number];
    //cout << sizeof(alumnos) << endl;
    //cout << sizeof(Alumno) << endl;
    /* Registrar las notas de los alumnos*/
    cout << "Registre las notas de los alumnos)" << endl; 
    for (int i=0 ; i<number;i++){
        cout << "Alumno " << i << endl;
        alumnos[i].pedirNotas();
        cout << endl;
    };
    /* Mostar las notas de los alumnos*/
    cout << "Mostrar las notas de los alumnos)" << endl; 
    for (int i=0 ; i<number;i++){
        cout << "Alumno " << i << endl;
        alumnos[i].mostrarNotas();
        cout << endl;
    };
    

    return 0;

}