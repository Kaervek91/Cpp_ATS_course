/*
*   BIRTHDAY FUNCTIONS
*/
#include "Clase_Alumno.h"
#include <iostream>
#include <string>


void Alumno::pedirNotas(){
    std::cout << "Introduce la calificación de Matemáticas ";
    std::cin >> calMate;
    std::cout << "Introduce la calificación de Programación ";
    std::cin >> calProgra;
}

void Alumno::mostrarNotas(){
    std::cout << "La calificación de Matemáticas " << calMate << std::endl;
    std::cout << "La calificación de Programación " << calProgra << std::endl;
    promedio = (calMate + calProgra)/2;
    std::cout << "La media es " << promedio << std::endl;
    
}