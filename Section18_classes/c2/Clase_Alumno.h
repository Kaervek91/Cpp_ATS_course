/* Birthday Class Definition Header */



class Alumno {
    private:
       float calMate, calProgra, promedio;

        bool isvalidDate(int _day,int _month);
        bool equal(int _day,int _month);

    public:
        Alumno(){ // inicializa los objetos, para crear arreglos de objetos necesitamos el constructor por defecto
        }
        // Pero siempre puedes poner otro constructor
        Alumno(float _calMate, float _calProgra){
            calMate = _calMate;
            calProgra= _calProgra;
        }
        
        void pedirNotas();
        void mostrarNotas();
};