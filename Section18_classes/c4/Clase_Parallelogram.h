/* Parallelogram Class Definition Header */
#include "iostream"

using namespace std;

class Parallelogram {
    private:
        float sideA = 0,sideB = 0,perimeter = 0,area = 0;

    public:
        Parallelogram(){
            this->sideA = 0;
            this->sideB = 0;
            obtainArea();
            obtainPerimeter();
        }
        Parallelogram(float sideA){ // inicializa los objetos, para crear arreglos de objetos necesitamos el constructor por defecto
            this->sideA = sideA;
            this->sideB = sideA;
            obtainArea();
            obtainPerimeter();
        }
        // Pero siempre puedes poner otro constructor
        Parallelogram(float sideA, float sideB){
            this->sideA = sideA;
            this->sideB = sideB;
            obtainArea();
            obtainPerimeter(); 
        }
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Parallelogram(){};
        void createObject(){
            float a,b;
            cout << "Insert sideA " << endl;
            cin >> a;
            setSideA(a);
            cout << "Insert sideB (introduce 0 if you want an square of side A)" << endl;
            cin >> b;
            setSideB(b);
        }
        float setSideA(float sideA){
            this->sideA = sideA;
            updateData();
        }

        float setSideB(float sideB){
            if(sideB != 0) {
                this->sideB = sideB;
            } else {
                this->sideB = this->sideA;
            }
            
            updateData();
        }
        float getSideA(){
            return this->sideA;
        };

        float getSideB(){
            return this->sideB;
        };

        float getArea(){
            return this->area;
        };

        float getPerimeter(){
            return this->perimeter;
        };

        void obtainPerimeter(){
            this->perimeter = (this->sideA + this->sideB )*2;
        };
        void obtainArea(){
            this->area = this->sideA*this->sideB;
        };

        void updateData(){
            obtainArea();
            obtainPerimeter();
        };

        void print(){
            cout << "\n\t ---  ";
            if (this->sideA == this->sideB) {
                cout << "Square";
            } else {
                cout << "Parallelogram";
            }
            cout << " data --- \n";
            cout << " - Side A " << this->sideA << endl;
            cout << " - Side B " << this->sideB << endl;
            cout << " - Area " << this->area << endl;
            cout << " - Perimeter " << this->perimeter << endl;
        };
        void menu(){
            cout << "hello\n";
        }
};