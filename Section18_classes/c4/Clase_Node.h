/* List Class Definition Header */

#ifndef NODE_H
#define NODE_H
#include "iostream"
#include "stdlib.h"



using namespace std;

template <class T>

class Node {
    
    public:
        // Método constructor
        Node();
        Node(T);
        // Método Destructor - se caracteriza porque no tiene ningún parámetro
        ~Node();
        
        //Atributos públicos
        Node *next;
        T data;

        // Métodos públicos
        void delete_all();
        void print();
};

                

#endif