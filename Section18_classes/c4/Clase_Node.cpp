/* 
    Funciones clase Node
    
    
    g++ -std=c++11 c1_classes.cpp Birthday.cpp -o c1_classes Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/

#include "Clase_Node.h"


using namespace std;

// Constructor por defecto
template<typename T>
 
Node<T>::Node()
{
    data = NULL;
    next = NULL;
}
 
// Constructor por parámetro
template<typename T>
Node<T>::Node(T data_)
{
    data = data_;
    next = NULL;
}
 
// Eliminar todos los Nodos
template<typename T>
void Node<T>::delete_all()
{
    if (next)
        next->delete_all();
    delete this;
}
 
// Imprimir un Nodo
template<typename T>
void Node<T>::print()
{
    //cout << "Node-> " << "Dato: " << dato << " Direcion: " << this << " Siguiente: " << next << endl;
    //cout << data << "-> ";
    data.print();
}
 
template<typename T>
Node<T>::~Node() {}

