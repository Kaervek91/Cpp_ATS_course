// Enter a char and determine if it is MAYUS or minus or is not a vocal
#include <iostream>
#include <ctype.h>

using namespace std;

int main () {
    char a ;
    cout<< "Enter char: "; cin >> a;
    if (isalpha(a) && islower(a)){
        switch(a)
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u': cout << "The letter is a vocal in lowercase"; break;
            default:  cout << "The letter is in lowercase but not vocal";       
        }
    }else{
        switch(a)
        {
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U': cout << "The letter is a vocal in UPPERCASE"; break;
            default:  cout << "The letter is in UPPERCASE but not vocal";       
        }
    }
    cout << "\n";
    return 0;
}