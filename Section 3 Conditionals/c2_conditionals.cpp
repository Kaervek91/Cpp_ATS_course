#include <iostream>

using namespace std;

int main () {

    int number , dato = 5;
    cout<< "Enter number between 1-5: "; cin >> number;

    switch(number){
        case 1:
            cout << "Number is 1";
            break;
        case 2:
            cout << "Number is 2";
            break;
        case 3:
            cout << "Number is 3";
            break;
        case 4:
            cout << "Number is 4";
            break;
        case 5:
            cout << "Number is 5";
            break;
        default:
            cout << "Number is outside range";
            break;
    }
    cout << "\n";
    return 0;
}