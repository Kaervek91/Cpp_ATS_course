// Enter a 3 digit, then ask for a fourth number and stablish if it repats one's previous entered
#include <iostream>

using namespace std;

int main () {
    int numbers[3];
    int last_number,i;
    for (i=0;i<3;i++){
        cout<< "Enter number " << (i+1) << " : "; cin >> numbers[i];
    };
    cout<< "Enter a new number "; cin >> last_number;
    for (i=0;i<3;i++){
        if (last_number == numbers[i]){
        cout << "The number is repeated from a previous one [" << last_number << "]\n";
        return 0;
        } 
    };
    cout << "The number is not repeated from a previous one [" << last_number << "]";
    cout << "\n";
    return 0;
}