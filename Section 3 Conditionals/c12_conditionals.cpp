/*
    SHOW MONTHS OF THE YEAR AND ASK A USER A NUMBER BETWEEN 1-12 TO SELECT ONE
*/
#include <iostream>

using namespace std;

int main () {
    string months[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
    int input,i,z;
    input = 0;
    z = 0;
    for (i=0;i<sizeof(months)/sizeof(months[0]);i++){
        z++;
        cout <<  z <<" - " << months[i] + "\n";
    };
    do {
        if  (input == 0){
            cout << "Enter the number of the month you want to print: ";cin >> input;
        }
        if (input >12 || input < 1){
            cout << "Enter a value between 1-12 to choose correctly a month\n";
            cout << "If you want to quit the program enter '0': "; cin >> input;
            if(input == 0){
                return 0;
            }
        } 
    } while (input >12 || input < 1);
    cout << "The month selected is: " << months[input] << endl ;
    return 0;
    
}