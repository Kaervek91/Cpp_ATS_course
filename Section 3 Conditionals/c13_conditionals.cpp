/*
    BUILD UP A CASHMACHINE WITH AN INITIAL 1000 $
*/
#include <iostream>


int EXIT_ACCOUNT = 0;
int CURRENT_ACCOUNT = 1000;


using namespace std;
void sum ();
void substract ();
void show_current_balance ();
void select_operation(void);
int check_pin(string pin, string* valid_pin, int array_length);

int main () {
    string valid_pin[] {"1234","0000","9999"};
    int array_length = sizeof(valid_pin)/sizeof(valid_pin[0]);
    //cout << valid_pin;
    string pin = "";
    do {
        cout << "Bienvenido introduzca su código Pin ";cin >> pin;
    } while(check_pin(pin, &valid_pin[0],array_length));
    do {
        select_operation();
         
    } while (!EXIT_ACCOUNT);
    return 0;
}
/*
    Function that check if the pin is inside database
*/
int check_pin(string pin,string* valid_pin, int array_length){
    int i;
    /*cout << "Valid_pin[0] " << valid_pin[0] << endl;
    cout << "Valid_pin " << valid_pin << endl;
    cout << "*Valid_pin " << *valid_pin << endl;
    cout << "&valid_pin " << &valid_pin << endl;
    cout << "VALOR FOR LIMIT " << array_length << endl;
    */
    for (i=0;i<array_length;i++){
        /*cout << "Valid_pin " << valid_pin << endl;
        cout << "*Valid_pin " << *valid_pin << endl;
        cout << "&valid_pin " << &valid_pin << endl;*/
        if (pin == *valid_pin) {
            cout << "PIN ACCEPTED\n";
            return 0;
        }
        valid_pin++;
    }
    cout << "ERROR INCORRECT PIN\n";
    return 1;
}
/*
    Function that offer the user several options
*/
void select_operation(){
    int select = 0;
    cout << "ALLOWED OPERATIONS:\n"
            "1- Whithdraw\n"
            "2- Deposit\n"
            "3- Check balance\n"
            "4- Exit\n"
            "Press one option --> [1-4] "; cin >> select;
    switch (select) {
        case 1: substract(); break;
        case 2: sum(); break;
        case 3: show_current_balance(); break;
        case 4: EXIT_ACCOUNT = 1;
        default: break;
    }

}

void substract(){
    int value;
    cout << "Enter the quantity you want to withdraw:\n"; cin >> value;
    if(value <= CURRENT_ACCOUNT){
        CURRENT_ACCOUNT -= value;
    } else {
        cout << "You do not have money enough in your account" << endl;
    }    
    show_current_balance();
}

void sum(){
    int value;
    cout << "Enter the quantity you want to deposit:\n"; cin >> value;
    CURRENT_ACCOUNT += value;
    show_current_balance();
}

void show_current_balance(){
    cout << "The actual balance is " << CURRENT_ACCOUNT << endl;
}