/*
   MENU WITH FOLLOWING OPTIONS:
   CASE 1 : CUBE OF A NUMBER
   CASE 2: OOD OR EVEN
   CASE 3: EXIT
*/
#include <iostream>
#include <math.h>

int EXIT_ACCOUNT = 0;

using namespace std;
void cube ();
void ood_even ();
void select_operation(void);


int main () {
    do {
        select_operation();
         
    } while (!EXIT_ACCOUNT);
    return 0;
}
/*
    Function that offer the user several options
*/
void select_operation(){
    int select = 0;
    cout << "ALLOWED OPERATIONS:\n"
            "1- Cube of a Number\n"
            "2- OOD OR EVEN\n"
            "3- Exit\n"
            "Press one option --> [1-3] "; cin >> select;
    switch (select) {
        case 1: cube(); break;
        case 2: ood_even(); break;
        case 3: EXIT_ACCOUNT = 1;
        default: break;
    }

}

void cube(){
    float value;
    cout << "Enter the value you want to elevate to the cube:\n"; cin >> value;
    if(!isnan(value)){
        cout << "The cube of " << value << " is " << pow(value,3) << endl;
    } else {
        cout << "You didn't insert a number" << endl;
    }
}

void ood_even(){
    int value;
    cout << "Enter the value you want to evaluate if is ood or even\n"; cin >> value;
    if(value%2 == 0){
        cout << "Is ood\n";
    } else {
        cout << "Is even\n";
    };
}
