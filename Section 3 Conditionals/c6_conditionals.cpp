// Enter a number and determine if it is positive, negative or null
#include <iostream>

using namespace std;

int main () {
    int a = 0;
    cout<< "Enter number: "; cin >> a;
    if (a > 0){
        cout << "The number is positive";
    }else if (a < 0){
        cout << "The number is negative";
    }else{
        cout << "The number is 0";
    }
    cout << "\n";
    return 0;
}