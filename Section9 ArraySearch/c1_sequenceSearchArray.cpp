/* 
    Metodo de busqueda secuencial
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(float number[], int n);
void search(float a[],float number,int first, int last);


int main(){
    string chain;
    
    float array[] = {5,2,3,6,1,8,7,0,6};
    float number = 6;
    
    
    search(array, number,0, 8);
    print (array,8);
    //print(number);
    return 0;

}

void print (float number[], int n){   
    int i;
    for (i=0;i<n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void search(float a[],float number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}