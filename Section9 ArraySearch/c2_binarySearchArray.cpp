/* 
    Metodo de busqueda binaria
    g++ -std=c++11 c5_loops.cpp -o c5_loops Necesario incluir el soporte de c++11 para evitar errores con la librería String
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace std;

void print(float number[], int n);
void search(float a[],float number,int first, int last);
void quickShort(float a[],int first, int last);
void binarySearch(float *array,float number,int inf,int sup);

int main(){
    string chain;
    
    float array[] = {5,2,3,13,1,8,7,0,6};
    float number = 6;
    
    quickShort(array,0,8);
    binarySearch(array,number,0,8);
    //search(array, number,0, 8);
    print (array,8);
    //print(number);
    return 0;

}

void print (float number[], int n){   
    int i;
    for (i=0;i<=n; i++){
        cout << number[i]<< ",";
    }
    cout << endl;
}

void intercambio(float &a, float &b){
    float aux;
    aux = a;
    a = b;
    b = aux;
}

void search(float a[],float number,int first, int last){
    int i,notFound;
    i = first;
    do {
        if (a[i] == number) {notFound = 0;}
        else{
            notFound = 1;
            i++;
        }
        
    }while(notFound && i<= last);
    
    if (!notFound) cout << "Number found in " << i << " with value " << a[i] << endl;
    if (notFound) cout << "Number not found" << endl;
    if (i < last){
        i++;
        search(a,number,i,last);
    }

}

void quickShort(float a[],int first, int last){
    int middle,i,j;
    float pivot;
    middle = (first + last )/2;
    pivot = a[middle];
    i = first;
    j = last;

    do{
        while(a[i]< pivot) i++;
        while(a[j]> pivot) j--;

        if (i<=j){
            intercambio(a[i], a[j]);
            i++;
            j--;
        }
    }while(i<=j);
    
    if (first < j){
        quickShort(a,first,j);
    }

    if (i < last){
        quickShort(a,i,last);
    }

}

void binarySearch(float *array,float number,int inf,int sup){
    int mitad,found = 0;
    
    while (inf <= sup){
        mitad = (inf+sup)/2;
        if (array[mitad] == number){
            found = 1;
            inf = mitad+1;
            break;
        }
        if (array[mitad] > number){
            sup = mitad;
            mitad = (inf+sup)/2;
        }
        if (array[mitad] < number){
            inf = mitad;
            mitad = (inf+sup)/2;
        }
    }
    if (found) {
        cout << "Number Found in position " << mitad << " with value " << number << endl;
        
    }
}